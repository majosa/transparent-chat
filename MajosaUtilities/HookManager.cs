﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Majosa
{
    public class HookManager
    {
        public enum HookType
        {
            WH_CALLWNDPROC     = 4,
            WH_CALLWNDPROCRET  = 12,
            WH_CBT             = 5,
            WH_DEBUG           = 9,
            WH_FOREGROUNDIDLE  = 11,
            WH_GETMESSAGE      = 3,
            WH_JOURNALPLAYBACK = 1,
            WH_JOURNALRECORD   = 0,
            WH_KEYBOARD        = 2,
            WH_KEYBOARD_LL     = 13,
            WH_MOUSE           = 7,
            WH_MOUSE_LL        = 14,
            WH_MSGFILTER       = -1,
            WH_SHELL           = 10,
            WH_SYSMSGFILTER    = 6
        }
        private Dictionary<IntPtr,HookProc> activeHooks=new Dictionary<IntPtr,HookProc>();
        private static HookManager instance;

        private HookManager()
        {
            activeHooks=new Dictionary<IntPtr,HookProc>();
        }

        public static HookManager Instance()
        {
            if (instance == null)
            {
                instance = new HookManager();
            }
            return instance;
        }

        public IntPtr AddHook(HookType type, HookProc proc)
        {
            if (activeHooks.ContainsValue(proc))
            {
                throw new Exception();
            }
            IntPtr id = SetWindowsHookEx((int)type, proc, GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName), 0);
            activeHooks.Add(id, proc);
            return id;
        }
        public void RemoveHook(IntPtr id)
        {
            UnhookWindowsHookEx(id);
            activeHooks.Remove(id);
        }
        public void RemoveHook(HookProc proc)
        {
            foreach (var i in activeHooks)
            {
                if (i.Value == proc)
                {
                    RemoveHook(i.Key);
                }
            }
        }
        ~HookManager()
        {
            foreach (var i in new List<IntPtr>(activeHooks.Keys))
            {
                RemoveHook(i);
            }
        }
        //Win32 imports
        public delegate IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);
    }

}
