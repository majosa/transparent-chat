﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Chat_streamer.ChatServices;

namespace Chat_streamer
{
    public class InstanceRun : DependencyObject
    {
        public static DependencyProperty ChannelProperty = DependencyProperty.Register("Channel", typeof(String), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty UsernameProperty = DependencyProperty.Register("Username", typeof(String), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(String), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty MainWindowProperty = DependencyProperty.Register("MainWindow", typeof(MainWindow), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty ClientProperty = DependencyProperty.Register("Client", typeof(Client), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty ConfigurationProperty = DependencyProperty.Register("Configuration", typeof(Configuration), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty EmoticonsProperty = DependencyProperty.Register("Emoticons", typeof(Emoticons), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty StringTablesProperty = DependencyProperty.Register("StringTables", typeof(DictionaryLanguage), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty BadgesProperty = DependencyProperty.Register("Badges", typeof(Badges), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty TranslatorProperty = DependencyProperty.Register("Translator", typeof(Translator.Translator), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        public static DependencyProperty ShowTranslationProperty = DependencyProperty.Register("ShowTranslation", typeof(bool), typeof(InstanceRun),
            new FrameworkPropertyMetadata());
        private static List<InstanceRun> listOfInstances = new List<InstanceRun>();

        public InstanceRun()
        {
            Configuration = Configuration.Load();
            //Configuration = new Chat_streamer.Configuration();
            StringTables = new DictionaryLanguage(this);
            Client = new TwitchClient("dummy");
            MainWindow = new MainWindow(this);
            
            this.ShowTranslation = Configuration.IsTranslationEnabled;
        }

        public String Channel
        {
            get { return (String)this.GetValue(InstanceRun.ChannelProperty); }
            set { this.SetValue(InstanceRun.ChannelProperty, value); }
        }
        public String Username
        {
            get { return (String)this.GetValue(InstanceRun.UsernameProperty); }
            set { this.SetValue(InstanceRun.UsernameProperty, value); }
        }
        public String Password
        {
            get { return (String)this.GetValue(InstanceRun.PasswordProperty); }
            set { this.SetValue(InstanceRun.PasswordProperty, value); }
        }
        public Configuration Configuration
        {
            get { return (Configuration)this.GetValue(InstanceRun.ConfigurationProperty); }
            set { this.SetValue(InstanceRun.ConfigurationProperty, value); }
        }
        public MainWindow MainWindow
        {
            get { return (MainWindow)this.GetValue(InstanceRun.MainWindowProperty); }
            set { this.SetValue(InstanceRun.MainWindowProperty, value); }
        }
        public Client Client
        {
            get { return (Client)this.GetValue(InstanceRun.ClientProperty); }
            set { this.SetValue(InstanceRun.ClientProperty, value); }
        }
        public Emoticons Emoticons
        {
            get { return (Emoticons)this.GetValue(InstanceRun.EmoticonsProperty); }
            set { this.SetValue(InstanceRun.EmoticonsProperty, value); }
        }
        public DictionaryLanguage StringTables
        {
            get { return (DictionaryLanguage)this.GetValue(InstanceRun.StringTablesProperty); }
            set { this.SetValue(InstanceRun.StringTablesProperty, value); }
        }
        public Badges Badges
        {
            get { return (Badges)this.GetValue(InstanceRun.BadgesProperty); }
            set { this.SetValue(InstanceRun.BadgesProperty, value); }
        }

        public bool ShowTranslation
        {
            get { return (bool)this.GetValue(InstanceRun.ShowTranslationProperty); }
            set { this.SetValue(InstanceRun.ShowTranslationProperty, value); }
        }

        public static InstanceRun GetMainInstance()
        {
            return listOfInstances[0];
        }

        [STAThread]
        public static int Main(String[] args)
        {
            Application app = new Application(); //URI to resources don't works if Application isn't intialized
            var main=new InstanceRun();
            listOfInstances.Add(main);
            app.Run(main.MainWindow);
            return 0;
        }
    }
}
