﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chat_streamer
{
    /// <summary>
    /// Lógica de interacción para Settings.xaml
    /// </summary>
    /// 

    public partial class Settings : Window
    {
        public event Action NewConnectionTry;
        public event Action<Configuration> NewSettings;

        public Settings()
        {
            InitializeComponent();
        }
        public Settings(String channel) : this()
        {
            ChannelBox.Text = channel;
        }
        public Settings(Configuration config) : this()
        {
            /*UserBox.Text = config.LastUsername;
            PasswordBox.Password = config.Password;
            ChannelBox.Text = config.Channel;
            TextColorPicker.SelectedColor = config.TextColor;
            BackgroundColorPicker.SelectedColor = config.BackgroundColor;
            CaptureCheckBox.IsChecked = config.IsEnabledF2;
            PreviewsCheckBox.IsChecked = config.IsEnabledPreviews;
            MaxMessagesUpDown.Value = config.MaxMessages;*/
        }

        private void onConnectButton(object sender, RoutedEventArgs e)
        {
            /*this.Close();
            NewConnectionTry();
            var settings = new Configuration();
            settings.UserName = UserBox.Text;
            settings.Password = PasswordBox.Password;
            settings.Channel = ChannelBox.Text;
            settings.TextColor = TextColorPicker.SelectedColor;
            settings.BackgroundColor = BackgroundColorPicker.SelectedColor;
            //settings.IsEnabledF2 = CaptureCheckBox.Checked;
            settings.IsEnabledF2 = (bool)CaptureCheckBox.IsChecked;
            settings.IsEnabledPreviews = (bool)PreviewsCheckBox.IsChecked;
            settings.MaxMessages = (int)MaxMessagesUpDown.Value;
            NewSettings(settings);*/
        }

        public String User { get { return UserBox.Text; } }
        public String Password { get { return PasswordBox.Password; } }
        public String Channel { get { return ChannelBox.Text; } }

        private void onClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void onExpandOptions(object sender, RoutedEventArgs e)
        {
            OptionsGrid.Visibility = Visibility.Visible;
            var anim=new DoubleAnimation(this.Height+OptionsGrid.ActualHeight,TimeSpan.FromSeconds(0.3));
            anim.EasingFunction = new CubicEase();
            this.BeginAnimation(Window.HeightProperty, anim);
        }

        private void onCollapseOptions(object sender, RoutedEventArgs e)
        {
            OptionsGrid.Visibility = Visibility.Hidden;
            var anim = new DoubleAnimation(this.Height - OptionsGrid.ActualHeight, TimeSpan.FromSeconds(0.3));
            anim.EasingFunction = new CubicEase();
            this.BeginAnimation(Window.HeightProperty, anim);
        }
    }
}
