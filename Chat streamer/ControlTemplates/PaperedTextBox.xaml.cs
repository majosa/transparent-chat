﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.ControlTemplates
{
    /// <summary>
    /// Lógica de interacción para PaperedTextBox.xaml
    /// </summary>
    public partial class PaperedTextBox : UserControl
    {
        public String WatermarkText
        {
            get { return ((String)GetValue(WatermarkTextProperty)); }
            set { SetValue(WatermarkTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WatermarkText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WatermarkTextProperty =
            DependencyProperty.Register("WatermarkText", typeof(String), typeof(PaperedTextBox), new PropertyMetadata("Field"));



        public String Regex
        {
            get { return (String)GetValue(RegexProperty); }
            set { SetValue(RegexProperty, value); }
        }

        public Regex RegexCompiled
        {
            get { return new Regex((String)GetValue(RegexProperty)); }
            set { SetValue(RegexProperty, value.ToString()); }
        }

        // Using a DependencyProperty as the backing store for Regex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegexProperty =
            DependencyProperty.Register("Regex", typeof(String), typeof(PaperedTextBox), new PropertyMetadata(".*"));

        

        public String Text
        {
            get { return (String)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(String), typeof(PaperedTextBox), new PropertyMetadata(""));

        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            ((Storyboard)MainGrid.Resources["WatermarkDisappears"]).Begin();
        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            if (!TextBox.IsFocused)
            {
                ((Storyboard)MainGrid.Resources["WatermarkAppears"]).Begin();
            }
        }

        private void onLostFocus(object sender, RoutedEventArgs e)
        {
            ((Storyboard)MainGrid.Resources["WatermarkAppears"]).Begin();
        }

        private void onGotFocus(object sender, RoutedEventArgs e)
        {

        }


        public PaperedTextBox()
        {
            InitializeComponent();
        }

        private String previousText="";
        private void onTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!this.RegexCompiled.IsMatch(TextBox.Text) && previousText!=TextBox.Text)
            {
                TextBox.Text = previousText;
            }
            else
            {
                previousText = TextBox.Text;
            }
        }


    }
}
