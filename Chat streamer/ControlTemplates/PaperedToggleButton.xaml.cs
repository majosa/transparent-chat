﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.ControlTemplates
{
    /// <summary>
    /// Lógica de interacción para PaperedToggleButton.xaml
    /// </summary>
    public partial class PaperedToggleButton : UserControl, INotifyPropertyChanged
    {
        #region External dependency properties
        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(bool), typeof(PaperedToggleButton), new PropertyMetadata(false));

        public Brush InactiveBrush
        {
            get { return (Brush)GetValue(InactiveBrushProperty); }
            set { SetValue(InactiveBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BorderColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InactiveBrushProperty =
            DependencyProperty.Register("InactiveBrush", typeof(Brush), typeof(PaperedToggleButton), new PropertyMetadata(Brushes.Gray));

        public Brush ActiveBrush
        {
            get { return (Brush)GetValue(ActiveBrushProperty); }
            set { SetValue(ActiveBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BorderColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActiveBrushProperty =
            DependencyProperty.Register("ActiveBrush", typeof(Brush), typeof(PaperedToggleButton), new PropertyMetadata(Brushes.Blue));
        #endregion

        #region Template dependency properties
        public Color BackgroundBrushTemplate
        {
            get { return (Color)GetValue(BackgroundBrushTemplateProperty.DependencyProperty); }
        }

        // Using a DependencyProperty as the backing store for CheckedColorTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey BackgroundBrushTemplateProperty =
            DependencyProperty.RegisterReadOnly("BackgroundBrushTemplate", typeof(Brush), typeof(PaperedToggleButton), new PropertyMetadata(Brushes.Transparent));

        public Color TextBrushTemplate
        {
            get { return (Color)GetValue(TextBrushTemplateProperty.DependencyProperty); }
        }

        // Using a DependencyProperty as the backing store for CheckedColorTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey TextBrushTemplateProperty =
            DependencyProperty.RegisterReadOnly("TextBrushTemplate", typeof(Brush), typeof(PaperedToggleButton), new PropertyMetadata(Brushes.Red));

        public Color BorderBrushTemplate
        {
            get { return (Color)GetValue(BorderBrushTemplateProperty.DependencyProperty); }
        }

        // Using a DependencyProperty as the backing store for CheckedColorTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey BorderBrushTemplateProperty =
            DependencyProperty.RegisterReadOnly("BorderBrushTemplate", typeof(Brush), typeof(PaperedToggleButton), new PropertyMetadata(Brushes.Gray));
        #endregion
        /*public Color InvertedBorderColor
        {
            get
            {
                var inv = new Color();
                inv.ScR = 1.0f - BorderColor.ScR;
                inv.ScB = 1.0f - BorderColor.ScB;
                inv.ScG = 1.0f - BorderColor.ScG;
                return inv;
            }
        }*/


        public PaperedToggleButton()
        {
            InitializeComponent();
            useUncheckedState();
        }

        private void useCheckedState()
        {
            this.SetValue(BackgroundBrushTemplateProperty, InactiveBrush);
            this.SetValue(TextBrushTemplateProperty, invertBrush(InactiveBrush));
        }

        private void useUncheckedState()
        {
            this.SetValue(BackgroundBrushTemplateProperty, Background);
            this.SetValue(TextBrushTemplateProperty, Foreground);
        }

        private void onMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsChecked)
            {
                IsChecked = false;
                useUncheckedState();
            }
            else
            {
                IsChecked = true;
                useCheckedState();
            }
        }

        private static Brush invertBrush(Brush origin)
        {
            Brush dest=null;
            if (origin is SolidColorBrush)
            {

                dest = new SolidColorBrush(invertColor(((SolidColorBrush)origin).Color));
            }
            return dest;
        }

        private static Color invertColor(Color origin)
        {
            var inv = new Color();
            inv.ScR = 1.0f - origin.ScR;
            inv.ScB = 1.0f - origin.ScB;
            inv.ScG = 1.0f - origin.ScG;
            inv.ScA = origin.ScA;
            return inv;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            this.SetValue(BorderBrushTemplateProperty, ActiveBrush);

        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            this.SetValue(BorderBrushTemplateProperty, InactiveBrush);
        }
    }
}
