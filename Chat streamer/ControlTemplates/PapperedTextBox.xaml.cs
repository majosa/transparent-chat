﻿using Majosa.Keyboard;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.ControlTemplates
{
    /// <summary>
    /// Lógica de interacción para PapperedTextBox.xaml
    /// </summary>
    public partial class PapperedTextBox : TextBox,INotifyPropertyChanged
    {


        public String Watermark
        {
            get { return (String)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Watermark.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register("Watermark", typeof(String), typeof(PapperedTextBox), new PropertyMetadata("Label"));



        public String Pattern
        {
            get { return (String)GetValue(PatternProperty); }
            set { SetValue(PatternProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Pattern.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PatternProperty =
            DependencyProperty.Register("Pattern", typeof(String), typeof(PapperedTextBox), new PropertyMetadata());

        public String ValidationMessage
        {
            get { return (String)GetValue(ValidationMessageProperty); }
            set { SetValue(ValidationMessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ValidationMessage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValidationMessageProperty =
            DependencyProperty.Register("ValidationMessage", typeof(String), typeof(PapperedTextBox), new PropertyMetadata());

        public bool IsPatternProgressive
        {
            get { return (bool)GetValue(IsPatternProgressiveProperty); }
            set { SetValue(IsPatternProgressiveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPatternProgressive.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPatternProgressiveProperty =
            DependencyProperty.Register("IsPatternProgressive", typeof(bool), typeof(PapperedTextBox), new PropertyMetadata(false));

        public String SanitizedText
        {
            get { return (String)GetValue(SanitizedTextProperty); }
            set { SetValue(SanitizedTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SanitizedText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SanitizedTextProperty =
            DependencyProperty.Register("SanitizedText", typeof(String), typeof(PapperedTextBox), new PropertyMetadata(""));

        

        public IEnumerable<object> AutocompleteItems
        {
            get { return (IEnumerable<object>)GetValue(AutocompleteItemsProperty); }
            set { SetValue(AutocompleteItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutocompleteItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutocompleteItemsProperty =
            DependencyProperty.Register("AutocompleteItems", typeof(IEnumerable<object>), typeof(PapperedTextBox), new PropertyMetadata());

        public DataTemplate AutocompleteItemTemplate
        {
            get { return (DataTemplate)GetValue(AutocompleteItemTemplateProperty); }
            set { SetValue(AutocompleteItemTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutocompleteItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutocompleteItemTemplateProperty =
            DependencyProperty.Register("AutocompleteItemTemplate", typeof(DataTemplate), typeof(PapperedTextBox), new PropertyMetadata());



        public bool OnlyAutocompleteValues
        {
            get { return (bool)GetValue(OnlyAutocompleteValuesProperty); }
            set { SetValue(OnlyAutocompleteValuesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OnlyAutocompleteValues.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OnlyAutocompleteValuesProperty =
            DependencyProperty.Register("OnlyAutocompleteValues", typeof(bool), typeof(PapperedTextBox), new PropertyMetadata(true));

        public Brush Foreground
        {
            get { return (Brush)GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForegroundProperty =
            DependencyProperty.Register("Foreground", typeof(Brush), typeof(PapperedTextBox), new PropertyMetadata(Brushes.Black,new PropertyChangedCallback(foregroundChanged)));

        private static void foregroundChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PapperedTextBox)d).WatermarkColor = ((SolidColorBrush)e.NewValue).Color;
            if (((PapperedTextBox)d).TextColor == ((SolidColorBrush)e.OldValue).Color)
            {
                ((PapperedTextBox)d).TextColor = ((SolidColorBrush)e.NewValue).Color;
            }
        }

        public Color WatermarkColor
        {
            get { return (Color)GetValue(WatermarkColorProperty); }
            set { SetValue(WatermarkColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WatermarkColorProperty =
            DependencyProperty.Register("WatermarkColor", typeof(Color), typeof(PapperedTextBox), new PropertyMetadata(Colors.Gray));

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        private static void textColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PapperedTextBox)d).SetValue(TextBox.ForegroundProperty,new SolidColorBrush((Color)e.NewValue));
        }

        // Using a DependencyProperty as the backing store for TextColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextColorProperty =
            DependencyProperty.Register("TextColor", typeof(Color), typeof(PapperedTextBox), new PropertyMetadata(Colors.Black,new PropertyChangedCallback(textColorChanged)));

        public bool PopupOpenedTemplate
        {
            get { return (bool)GetValue(PopupOpenedTemplateProperty.DependencyProperty); }
        }

        // Using a DependencyProperty as the backing store for WatermarkBrushTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey PopupOpenedTemplateProperty =
            DependencyProperty.RegisterReadOnly("PopupOpenedTemplate", typeof(bool), typeof(PapperedTextBox), new PropertyMetadata(false));

        public Brush BorderBrushTemplate
        {
            get { return (Brush)GetValue(BorderBrushTemplateProperty.DependencyProperty); }
        }

        // Using a DependencyProperty as the backing store for WatermarkBrushTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey BorderBrushTemplateProperty =
            DependencyProperty.RegisterReadOnly("BorderBrushTemplate", typeof(Brush), typeof(PapperedTextBox), new PropertyMetadata(new SolidColorBrush(Colors.LightBlue)));



        public String WildcardChar
        {
            get { return (String)GetValue(WildcardCharProperty); }
            set { SetValue(WildcardCharProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WildcardChar.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WildcardCharProperty =
            DependencyProperty.Register("WildcardChar", typeof(String), typeof(PapperedTextBox), new PropertyMetadata(""));

        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly RoutedEvent TextChangedEvent = EventManager.RegisterRoutedEvent("TextChanged", RoutingStrategy.Bubble, typeof(TextChangedEventHandler), typeof(PapperedTextBox));

        public event TextChangedEventHandler TextChanged
        {
            add { this.AddHandler(TextChangedEvent, value); }
            remove { this.RemoveHandler(TextChangedEvent, value); }
        }

        public String Text
        {
            get
            {
                return this.internalText;
            }
            set
            {
                if (value == null)
                {
                    this.internalText = "";
                    return;
                }
                if (this.WildcardChar != "")
                {
                    var wilded = "";
                    for (uint i = 0; i < value.Count(); i++)
                    {
                        wilded += this.WildcardChar;
                    }
                    this.SetValue(TextBox.TextProperty, wilded);
                }
                else
                {
                    this.SetValue(TextBox.TextProperty, value);
                }
                this.internalText = value;
                this.SanitizedText = value;
            }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty TextProperty =
        //    DependencyProperty.Register("Text", typeof(String), typeof(PapperedTextBox), new PropertyMetadata("",new PropertyChangedCallback(onTextPropertyChanged)));

        //private static void onTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    if (((PapperedTextBox)d).WildcardChar != "")
        //    {
        //        var wilded = "";
        //        for (uint i = 0; i < ((String)e.NewValue).Count(); i++)
        //        {
        //            wilded += ((PapperedTextBox)d).WildcardChar;
        //        }
        //        ((PapperedTextBox)d).SetValue(TextBox.TextProperty, wilded);
        //    }
        //    else
        //    {
        //        var pt = ((PapperedTextBox)d).SelectionStart;
        //        ((PapperedTextBox)d).SetValue(TextBox.TextProperty, e.NewValue);
        //        ((PapperedTextBox)d).internalText = (String)e.NewValue;
        //    }
        //}        

        private PapperedListBox AutocompleteList
        {
            get { return (PapperedListBox)this.Template.FindName("AutocompleteList",this); }
        }

        private Popup AutocompletePopup
        {
            get { return (Popup)this.Template.FindName("AutocompletePopup", this); }
        }

        private bool ignoreEntry = false;
        private bool selectionisasuggestion = false;
        private bool autocompleteselectionbykeys = false;
        private String internalText = "";

        public PapperedTextBox()
        {
            InitializeComponent();
            this.WatermarkColor = ((SolidColorBrush)this.Foreground).Color;
            this.PropertyChanged+=new PropertyChangedEventHandler(delegate(Object sender,PropertyChangedEventArgs ev){});
            //Some characters like Space are proccesed on keyDown, so we need a handler who accepts replies for get these characters
            //this.SetValue(BorderBrushProperty, new SolidColorBrush(Colors.Gray));
        }

        private void refreshAutocomplete(String mask = "",bool updateSuggestion=true)
        {
            Console.WriteLine("Call");
            AutocompleteList.Items.Clear();
            if (this.AutocompleteItems==null || this.AutocompleteItems.Count() == 0) return;
            bool autocompletedChannel = false;
            selectionisasuggestion = false;
            foreach (var i in this.AutocompleteItems)
            {
                var pos = i.ToString().IndexOf(mask);
                if (pos > -1)
                {
                    var item = new ListBoxItem();
                    if (this.AutocompleteItemTemplate != null)
                    {
                        item.Content = this.AutocompleteItemTemplate.LoadContent();
                        item.DataContext = i;
                    }
                    else
                    {
                        item.Content = i;
                    }
                    item.HorizontalContentAlignment = this.HorizontalContentAlignment;
                    item.VerticalContentAlignment = this.VerticalContentAlignment;
                    //item.Foreground = this.Foreground;
                    AutocompleteList.Items.Add(item);
                    if (pos == 0 && !autocompletedChannel && updateSuggestion)
                    {
                        base.Text = i.ToString();
                        this.SelectionStart = mask.Count();
                        this.SelectionLength = i.ToString().Count() - mask.Count();
                        selectionisasuggestion = true;
                        autocompletedChannel = true;
                    }
                }
            }
            if (!autocompletedChannel && this.OnlyAutocompleteValues && updateSuggestion)
            {
                if (mask.Length == 0)
                {
                    refreshAutocomplete();
                }
                else
                {
                    refreshAutocomplete(mask.Substring(0, mask.Count() - 1));
                }
            }
            //Sync text
            //this.internalText = base.Text;
            refreshChannelListHeight();
        }

        private void refreshChannelListHeight()
        {
            Console.WriteLine("Call height");
            var originalHeight = AutocompletePopup.MaxHeight;
            AutocompletePopup.BeginAnimation(ListBox.MaxHeightProperty, null);
            AutocompletePopup.MaxHeight = 150.0;
            AutocompleteList.Measure(new Size(this.ActualWidth, (this.IsFocused) ? 400.0 : 0.0));
            AutocompletePopup.MaxHeight = originalHeight;
            if (AutocompletePopup.MaxHeight != AutocompleteList.DesiredSize.Height)
            {
                AutocompletePopup.BeginAnimation(Popup.MaxHeightProperty, new DoubleAnimation(AutocompleteList.DesiredSize.Height, TimeSpan.FromSeconds(0.3)));
            }
        }

        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            if (!IsFocused) ((Storyboard)this.Resources["FocusControl"]).Begin(this, this.Template);
        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            if(!IsFocused) ((Storyboard)this.Resources["UnfocusControl"]).Begin(this, this.Template);
        }

        private void onGotFocus(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Call Focus");
            //KeyboardManager.Instance().KeyPressed += onKeyDown;
            //KeyboardManager.Instance().KeyUp += onKeyUp;
            if (!AutocompletePopup.IsMouseOver)
            {
                ((Storyboard)this.Resources["FocusControl"]).Begin(this, this.Template);
                if (AutocompleteItems != null)
                {
                    this.SetValue(PopupOpenedTemplateProperty, true);
                    refreshAutocomplete(updateSuggestion:false);
                    this.SelectionStart = 0;
                    this.SelectionLength = this.internalText.Count();
                }
            }
        }

        private void onLostFocus(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Call Unfocus");
            //KeyboardManager.Instance().KeyPressed -= onKeyDown;
            //KeyboardManager.Instance().KeyUp -= onKeyUp;
            if (!AutocompletePopup.IsMouseOver)
            {
                if (AutocompleteItems != null)
                {
                    this.SetValue(PopupOpenedTemplateProperty, false);
                    refreshAutocomplete(updateSuggestion: false);
                }
                if (!this.IsMouseOver) ((Storyboard)this.Resources["UnfocusControl"]).Begin(this, this.Template);
            }
        }

        private void onAutocompleteSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Console.WriteLine("Call Selected");
            if(((ListBox)sender).SelectedItem is ListBoxItem){
                if (AutocompleteItemTemplate != null)
                {
                    base.Text = ((ListBoxItem)((ListBox)sender).SelectedItem).DataContext.ToString();
                }
                else
                {
                    base.Text = ((ListBoxItem)((ListBox)sender).SelectedItem).Content.ToString();
                }
                this.SelectionStart = 0;
                this.SelectionLength = base.Text.Count();
            }
            if (autocompleteselectionbykeys)
            {
                this.internalText = base.Text;
                autocompleteselectionbykeys = false;
            }
            else
            {
                this.internalText = base.Text;
                refreshAutocomplete(base.Text);
            }
            RaiseEvent(new TextChangedEventArgs(PapperedTextBox.TextChangedEvent, UndoAction.Create));
        }

        private void onTextChanged(object sender, TextChangedEventArgs e)
        {
            //refreshAutocomplete(this.internalText);
        }

        private void onKeyUp(object sender, GlobalKeyEventArgs e)
        {
            return;
            //Console.WriteLine("Call Key "+e.Key);
            if (AutocompleteItems != null)
            {
                if (e.Key == Key.Back && this.internalText.Count() > 0)
                {
                    if (this.internalText.Count() > 0)
                    {
                        refreshAutocomplete(this.internalText.Substring(0, this.internalText.Count() - 1));
                    }
                    else
                    {
                        refreshAutocomplete();
                    }
                }
                
                else if (e.Key == Key.Tab)
                {
                    refreshChannelListHeight();
                }
                else if (e.Key.ToString().Count() == 1 || e.Key == Key.Space)
                {
                    refreshAutocomplete(this.internalText);
                }
            }
            //Sync text
            this.internalText=(String)base.GetValue(TextBox.TextProperty);
        }

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine("Call down");
            if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl) ignoreEntry = true;
            if (e.Key == Key.Back)
            {
                Console.WriteLine("Call back");
                if (this.SelectionLength > 0)
                {
                    if (this.SelectionStart>0 && selectionisasuggestion)
                    {
                        this.SelectionStart--;
                        this.SelectionLength++;
                    }
                    this.internalText = this.internalText.Substring(0, this.SelectionStart) + this.internalText.Substring(this.SelectionStart + this.SelectionLength);           
                }
                else if(this.SelectionStart>0)
                {
                    this.internalText = this.internalText.Substring(0, this.SelectionStart-1) + this.internalText.Substring(this.SelectionStart);
                }
                //Console.WriteLine(this.internalText);
            }
            else if (e.Key == Key.Down && AutocompleteList.SelectedIndex < AutocompleteList.Items.Count)
            {
                autocompleteselectionbykeys = true;
                AutocompleteList.SelectedIndex++;
                e.Handled = true;
            }
            else if (e.Key == Key.Up && AutocompleteList.SelectedIndex > 0)
            {
                autocompleteselectionbykeys = true;
                AutocompleteList.SelectedIndex--;
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                if (this.SelectionLength > 0)
                {
                    this.internalText = this.internalText.Substring(0, this.SelectionStart) + " " + this.internalText.Substring(this.SelectionStart + this.SelectionLength);
                }
                else
                {
                    this.internalText = this.internalText.Substring(0, this.SelectionStart) + " " + this.internalText.Substring(this.SelectionStart);
                }
                refreshAutocomplete(this.internalText);
                if (WildcardChar != "")
                {
                    var pointer = this.SelectionStart;
                    if (this.SelectionLength > 0)
                    {
                        base.Text = base.Text.Substring(0, this.SelectionStart) + WildcardChar + base.Text.Substring(this.SelectionStart + this.SelectionLength);
                    }
                    else
                    {
                        base.Text = base.Text.Substring(0, this.SelectionStart) + WildcardChar + base.Text.Substring(this.SelectionStart);
                    }
                    this.SelectionStart = pointer+1;
                    e.Handled = true;
                }
                if (this.Pattern != null)
                {
                    if (new Regex(this.Pattern).IsMatch(this.internalText))
                    {
                        SanitizedText = this.internalText;
                    }
                    else
                    {
                        this.internalText = SanitizedText;
                        base.Text = this.internalText;
                        e.Handled = true;
                    }
                }
            }
            
            
        }

        private void onCharKeyPressed(object sender, TextCompositionEventArgs e)
        {
            //Console.WriteLine("Call press");
            if (this.SelectionLength > 0)
            {
                this.internalText = this.internalText.Substring(0, this.SelectionStart) + e.Text + this.internalText.Substring(this.SelectionStart + this.SelectionLength);
            }
            else
            {
                //Console.WriteLine(this.internalText.Length);
                this.internalText = this.internalText.Substring(0, this.SelectionStart) + e.Text + this.internalText.Substring(this.SelectionStart);
            }
            Console.WriteLine(this.internalText);
            if (WildcardChar != "")
            {
                var pointer = this.SelectionStart;
                if (this.SelectionLength > 0)
                {
                    base.Text = base.Text.Substring(0, this.SelectionStart) + WildcardChar + base.Text.Substring(this.SelectionStart + this.SelectionLength);
                }
                else
                {
                    //Console.WriteLine(this.internalText.Length);
                    base.Text = base.Text.Substring(0, this.SelectionStart) + WildcardChar + base.Text.Substring(this.SelectionStart);
                }
                this.SelectionStart = pointer + 1;
                e.Handled = true;
            }
            if (this.Pattern != null)
            {
                if (new Regex(this.Pattern).IsMatch(this.internalText))
                {
                    SanitizedText = this.internalText;
                }
                else
                {
                    this.internalText = SanitizedText;
                    base.Text = this.internalText;
                    e.Handled = true;
                }
            }
        }

        private void onKeyUp(object sender, KeyEventArgs e)
        {
            //Console.WriteLine("Call up");
            if (WildcardChar == "" && !ignoreEntry && e.Key!=Key.Up && e.Key!=Key.Down &&
                this.AutocompleteItems!=null && this.AutocompleteItems.Count()!=0)
            {
                //All events got processed, we can do the autocompletetion
                refreshAutocomplete(this.internalText);
                //Sync sanitized text
                this.internalText = base.Text;
                //e.Handled = true;
            }
            if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl) ignoreEntry = false;
            this.PropertyChanged(this,new PropertyChangedEventArgs("Text"));
            RaiseEvent(new TextChangedEventArgs(PapperedTextBox.TextChangedEvent, UndoAction.Create));
        }

        private void onSelectionChanged(object sender, RoutedEventArgs e)
        {
            selectionisasuggestion = false;
        }
    }
}
