﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.ControlTemplates
{
    /// <summary>
    /// Lógica de interacción para PapperedListBoxItem.xaml
    /// </summary>
    public partial class PapperedListBoxItem : ListBoxItem
    {
        #region Template dependency properties


        public Brush BackgroundBrushTemplate
        {
            get { return (Brush)GetValue(BackgroundBrushTemplateProperty.DependencyProperty); }
        }

        // Using a DependencyProperty as the backing store for BackgroundBrushTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey BackgroundBrushTemplateProperty =
            DependencyProperty.RegisterReadOnly("BackgroundBrushTemplate", typeof(Brush), typeof(PapperedListBoxItem), new PropertyMetadata(Brushes.Transparent));

        
        #endregion

        public PapperedListBoxItem()
        {
            InitializeComponent();
        }

        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            if (!IsSelected) this.SetValue(BackgroundBrushTemplateProperty, replaceAlfa(BorderBrush, 0.5f));
        }

        private static Brush replaceAlfa(Brush origin, float alpha)
        {
            if (origin is SolidColorBrush)
            {
                return new SolidColorBrush(replaceAlfa(((SolidColorBrush)origin).Color,alpha));
            }
            return null;
        }

        private static Color replaceAlfa(Color origin, float alpha)
        {
            var nColor = origin;
            nColor.ScA = alpha;
            return nColor;
        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            if(!IsSelected) this.SetValue(BackgroundBrushTemplateProperty, Brushes.Transparent);
        }

        private void onSelected(object sender, RoutedEventArgs e)
        {
            this.SetValue(BackgroundBrushTemplateProperty, (IsSelected) ? BorderBrush : Brushes.Transparent);
        }
    }
}
