﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.ControlTemplates
{
    /// <summary>
    /// Lógica de interacción para PapperedButton.xaml
    /// </summary>
    public partial class PapperedButton : Button
    {


        public bool IsToggleButton
        {
            get { return (bool)GetValue(IsToggleButtonProperty); }
            set { SetValue(IsToggleButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsToggleButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsToggleButtonProperty =
            DependencyProperty.Register("IsToggleButton", typeof(bool), typeof(PapperedButton), new PropertyMetadata(false));



        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCheckedProperty =
            DependencyProperty.Register("IsChecked", typeof(bool), typeof(PapperedButton), new PropertyMetadata(false,onCheckedChanged));

        private static void onCheckedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (((PapperedButton)d).backgroundTemplate!=null) ((PapperedButton)d).updateState();
        }



        public bool IsInProgress
        {
            get { return (bool)GetValue(IsInProgressProperty); }
            set { SetValue(IsInProgressProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsInProgress.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsInProgressProperty =
            DependencyProperty.Register("IsInProgress", typeof(bool), typeof(PapperedButton), new PropertyMetadata(false));

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextColorProperty =
            DependencyProperty.Register("TextColor", typeof(Color), typeof(PapperedButton), new PropertyMetadata(Colors.Black));

        private Border borderTemplate
        {
            get { return (Border)this.Template.FindName("PART_Border", this); }
        }

        private Rectangle backgroundTemplate
        {
            get { return (Rectangle)this.Template.FindName("PART_Background", this); }
        }

        public PapperedButton()
        {
            InitializeComponent();
            //borderTemplate.BorderBrush = new SolidColorBrush(((SolidColorBrush)this.BorderBrush).Color);
        }

        private void updateState()
        {
            if (IsToggleButton)
            {
                backgroundTemplate.BeginAnimation(Rectangle.OpacityProperty, null);
                backgroundTemplate.Opacity = (IsChecked) ? 1.0 : 0.7;
            }
        }

        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            if(!IsChecked) ((Storyboard)this.Resources["FocusControl"]).Begin(this, this.Template);
        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            if (!IsChecked) ((Storyboard)this.Resources["UnfocusControl"]).Begin(this, this.Template);
        }

        private void onMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsToggleButton)
            {
                updateState();
                IsChecked = !IsChecked;
            }
            else
            {
                backgroundTemplate.Fill = Brushes.Black;
            }
        }

        private void onMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!IsToggleButton)
            {
                backgroundTemplate.Fill = this.BorderBrush;
            }
        }
    }
}
