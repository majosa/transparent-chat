﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.ControlTemplates
{
    /// <summary>
    /// Lógica de interacción para PapperedListBox.xaml
    /// </summary>
    public partial class PapperedListBox : ListBox
    {
        public PapperedListBox()
        {
            InitializeComponent();
        }
    }
}
