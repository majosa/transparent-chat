﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace Chat_streamer.ControlTemplates
{
    public class PapperedColorPicker : PapperedTextBox
    {

        public Color SelectedColor
        {
            get { return (Color)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedColorProperty =
            DependencyProperty.Register("SelectedColor", typeof(Color), typeof(PapperedColorPicker), new PropertyMetadata(Colors.Black,onSelectedColorChanged));

        private static void onSelectedColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PapperedColorPicker)d).TextColor = (Color)e.NewValue;
            ((PapperedColorPicker)d).Text = ((Color)e.NewValue).ToString();
        }

        private Popup ColorPickerPopup
        {
            get { return (Popup)this.Template.FindName("ColorPickerPopup", this); }
        }

        public PapperedColorPicker()
        {
            InitializeComponent();
            this.Text = "#FF000000";
            ResourceDictionary templates = new ResourceDictionary { Source = new Uri(@"\ControlTemplates\Templates.xaml", UriKind.Relative) };
            this.Template = (ControlTemplate)templates["PapperedColorPickerTemplate"];
            this.Pattern = "#[A-Z]+";
            this.GotFocus += onGotFocus;
            this.LostFocus += onLostFocus;
            var bForeground = new SolidColorBrush();
            //ColorPickerPopup.MouseLeave += onPopupMouseLeave;
        }

        private void onMouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void onLostFocus(object sender, RoutedEventArgs e)
        {
            ColorPickerPopup.IsOpen = false;
        }

        private void onGotFocus(object sender, RoutedEventArgs e)
        {
            ColorPickerPopup.IsOpen = true;
        }

        private void onPopupMouseLeave(object sender, MouseEventArgs e)
        {
            if (!this.IsFocused)
            {

                ColorPickerPopup.IsOpen = false;
            }
        }
    }
}
