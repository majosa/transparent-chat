﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Markup;

namespace Chat_streamer.ControlTemplates
{
    public class AplhedColorConverter : MarkupExtension, IValueConverter
    {
        public AplhedColorConverter()
        {

        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Color origin = (Color)value;
            origin.ScA = float.Parse((String)parameter);
            return origin;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Color origin = (Color)value;
            origin.ScA = 1.0f;
            return origin;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new AplhedColorConverter();
        }
    }

    public class IntegerToThickness : MarkupExtension, IValueConverter
    {
        public IntegerToThickness()
        {

        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is String)
            {
                if (value == "")
                {
                    value = 0.0;
                }else{
                    value = Double.Parse((String)value);
                }
            }
            return new Thickness((double)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((Thickness)value).Left;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new IntegerToThickness();
        }
    }

    public class ColorToSolidBrush : MarkupExtension, IValueConverter
    {
        public ColorToSolidBrush()
        {

        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is String)
            {
                if (value == "")
                {
                    value = new Color();
                }
                else
                {
                    value = ColorConverter.ConvertFromString((String)value);
                }
            }
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((SolidColorBrush)value).Color;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new ColorToSolidBrush();
        }
    }

    public class ContrastedColor : MarkupExtension, IValueConverter
    {
        public ContrastedColor()
        {

        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Color origin = (Color)value;
            return (origin.ScA>0.7 || origin.ScG>0.7 || origin.ScB>0.7) ? Colors.White : Colors.Black;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new ContrastedColor();
        }
    }
}
