﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IrcDotNet;
using Majosa.Keyboard;
using Chat_streamer.ChatServices;
using Chat_streamer.Dialogs;

namespace Chat_streamer
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private InstanceRun instance;
        private Client transitionIrc;
        private bool isFocusedOnMessageBox;
        private ChannelPicker channelPicker;
        private bool keepToEnd;

        #region Status vars
        private bool guestMode = true;
        #endregion

        private const String CHANNEL = "nochannel";

        private delegate void NotifyMessageDelegate(String message);

        public MainWindow(InstanceRun instance)
        {
            this.instance = instance;
            this.DataContext = instance;
            InitializeComponent();
            this.Resources.MergedDictionaries.Add(instance.StringTables);
            printInfoMessage("CloseIndications");
            //Install hooks
            KeyboardManager.Instance().KeyPressed += onGlobalKeyPressed;
            KeyboardManager.Instance().KeyUp += onGlobalKeyUp;
            showChannelPicker();
        }

        private void connectUsingInstance()
        {
            Client cl = instance.Channel == "yobavideogames" ? (Client)new LexekClient(instance.Channel, instance.Username, instance.Password) : (Client)new TwitchClient(instance.Channel, instance.Username, instance.Password);
            passClientControl(cl);
        }

        private void passClientControl(Client tr)
        {
            this.instance.Client = tr;
            this.instance.Client.Connected += onConnected;
            this.instance.Client.Joined += onJoinedOnChannel;
            this.instance.Client.Logged += onLogged;
            this.instance.Client.MutedUser += onMuteRequest;
            this.instance.Client.ChannelMessageReceived += onChannelMessageReceived;
        }

        private void onConnected()
        {
            printInfoMessage("ConnectedIndication");
        }

        private void disconnect()
        {
            this.instance.Client.Connected -= onConnected;
            this.instance.Client.Joined -= onJoinedOnChannel;
            this.instance.Client.Logged -= onLogged;
            this.instance.Client.MutedUser -= onMuteRequest;
            this.instance.Client.ChannelMessageReceived -= onChannelMessageReceived;
            this.instance.Client.Disconnect();
            this.instance.Client = null;
        }

        private void turnVisble(object sender, MouseEventArgs e)
        {
            //this.WindowStyle = WindowStyle.SingleBorderWindow;
            SolidColorBrush newBackground = new SolidColorBrush(instance.Configuration.BackgroundColor);
            this.Background = newBackground;
            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            ResizeRect.Opacity = 1.0;
            MoveRect.Opacity = 1.0;
            Color borderColor = instance.Configuration.BorderColor;
            borderColor.ScA = 1.0f;
            ScrollBorder.BorderBrush = new SolidColorBrush(borderColor);
            MessageBox.Opacity = 1.0;
            //ChatBox.ScrollToEnd();
        }

        private void turnHidden(object sender, MouseEventArgs e)
        {
            //this.WindowStyle = WindowStyle.None;
            SolidColorBrush newBackground = new SolidColorBrush(instance.Configuration.BackgroundColor);
            Color newColor = newBackground.Color;
            newColor.ScA = 0F;
            newBackground.Color = newColor;
            this.Background = newBackground;
            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            ResizeRect.Opacity = 0.1;
            Color borderColor = instance.Configuration.BorderColor;
            borderColor.ScA = 0.1f;
            ScrollBorder.BorderBrush = new SolidColorBrush(borderColor);
            MoveRect.Opacity = 0.1;
            if (!isFocusedOnMessageBox) MessageBox.Opacity = 0.1;
            //ChatBox.ScrollToEnd();
        }

        private void showChannelPicker(bool signinMode=false)
        {
            this.channelPicker = new ChannelPicker(instance,signinMode);
            this.channelPicker.ConnectionRequested += onChannelVarsChanged;
            this.channelPicker.ShowDialog();
        }

        void sendMessageFromMessageBox()
        {
            if (MessageBox.Text == "/close")
            {
                onClose(null, null);
            }
            else
            {
                this.instance.Client.SendMessageToChannel(MessageBox.Text);
                printMessage(this.instance.Client.GetUser(this.instance.Username), MessageBox.Text);
                //PrintMessage(this.instance.Client.GetUserName(), Colors.Black, MessageBox.Text);
                MessageBox.Text = "";
            }
        }

        void printInfoMessage(String info, bool error)
        {
            var messageInline = new Run(info);
            if (error)
            {
                messageInline.Foreground = new SolidColorBrush(Colors.DarkRed);
                messageInline.FontWeight = FontWeights.SemiBold;
            }
            ChatBox.Document.Blocks.Add(new Paragraph(messageInline));
        }

        void printInfoMessage(String key, String[] param=null)
        {
            if (this.Dispatcher.Thread != System.Threading.Thread.CurrentThread)
            {
                this.Dispatcher.Invoke(new Action<String, String[]>(printInfoMessage), new Object[] { key, param });
                return;
            }
            ChatBox.Document.Blocks.Add(new InfoMessage(instance, key, param));
        }

        void muteMessages(User user)
        {
            foreach (Block i in ChatBox.Document.Blocks)
            {
                if (i is MessageParagraph)
                {
                    var message = (MessageParagraph)i;
                    if (message.GetOwner() == user.UserName) message.EraseMessage();
                }
            }
        }

        void regenerateIcons()
        {
            if (instance.Channel == "yobavideogames")
            {
                instance.Emoticons = new TwitchEmoticons("yobavgc");
                instance.Badges = new TwitchBadges("yobavgc");
            }
            else
            {
                instance.Emoticons = new TwitchEmoticons(instance.Channel);
                instance.Badges = new TwitchBadges(instance.Channel);
            }
        }

        void leaveOnGuestMode()
        {
            MessageArea.Visibility = Visibility.Hidden;
            MessageBox.IsEnabled = true;
            MessageBox.Text = "";
            guestMode = false;
        }

        void enterOnGuestMode()
        {
            MessageArea.Visibility = Visibility.Visible;
            MessageBox.IsEnabled = false;
            MessageBox.Text = (String)instance.StringTables["JoinIndication"];
            guestMode = true;
        }

        void syncGuestModeWithClient()
        {
            if (instance.Client.IsGuest)
            {
                enterOnGuestMode();
            }
            else
            {
                leaveOnGuestMode();
            }
        }

        double movedOffset = 0.0;

        private void printMessage(User owner, String message)
        {
            //Delete previous messages
            movedOffset = 0.0;
            if (ChatBox.Document.Blocks.Count + 1 > this.instance.Configuration.MaxMessages)
            {
                var messagesToDelete = ChatBox.Document.Blocks.Count + 1 - this.instance.Configuration.MaxMessages;
                for (var i = 0; i < messagesToDelete; i++)
                {
                    var paragraph = ChatBox.Document.Blocks.ElementAt(0);
                    if (paragraph is MessageParagraph)
                    {
                        ((MessageParagraph)paragraph).PrepareToBeDeleted();
                    }
                    movedOffset += paragraph.ElementStart.GetCharacterRect(LogicalDirection.Forward).Bottom -
                        paragraph.ElementStart.GetCharacterRect(LogicalDirection.Forward).Top + 3;
                    ChatBox.Document.Blocks.Remove(paragraph);
                }
            }
            //Scroller.ScrollToVerticalOffset(Scroller.VerticalOffset - movedOffset);
            if (ChatBox.Document.Blocks.Count > 0 &&
                ChatBox.Document.Blocks.LastBlock is MessageParagraph &&
                ((MessageParagraph)ChatBox.Document.Blocks.LastBlock).GetOwner() == owner.UserName)
            {
                ((MessageParagraph)ChatBox.Document.Blocks.LastBlock).AddText("\n" + message);
            }
            else
            {
                ChatBox.Document.Blocks.Add(new MessageParagraph(instance, owner, message));
            }
            if (keepToEnd) Scroller.ScrollToEnd();
            //ChatBox.ScrollToEnd();
        }

        private void insertCharacterOnMessageBox(String character)
        {
            var cur = MessageBox.SelectionStart;
            MessageBox.Text = MessageBox.Text.Insert(cur, character);
            MessageBox.SelectionStart = cur + 1;
        }

        private void removeCharacterOnMessageBox()
        {
            var cur = MessageBox.SelectionStart;
            if (cur > 0)
            {
                MessageBox.Text = MessageBox.Text.Remove(cur - 1, 1);
                MessageBox.SelectionStart = cur - 1;
            }
        }

        private void enterOnCaptureMode()
        {
            isCapturing = true;
            MessageBox.IsReadOnly = true;
            MessageBox.Focus();
            MessageBox.Opacity = 1.0;
        }

        private void leaveOnCaptureMode()
        {
            isCapturing = false;
            MessageBox.IsReadOnly = false;
            MessageBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            turnHidden(null, null);
        }

        private void moveSelectorOnMessageBox(bool right)
        {
            if (!right)
            {
                if (MessageBox.SelectionStart > 0) MessageBox.SelectionStart--;
            }
            else
            {
                MessageBox.Focus();
                if (MessageBox.SelectionStart < MessageBox.Text.Count()) MessageBox.SelectionStart++;
            }
        }

        private void passToUseTrasitionClient()
        {
            var luser = instance.Client.Username;
            var lchannel = instance.Client.ChannelName;
            disconnect();
            passClientControl(transitionIrc);
            transitionIrc = null;
            regenerateIcons();
        }

        private void saveChannelOnConfig()
        {
            if (instance.Configuration.LastChannels.Contains(instance.Channel))
            {
                instance.Configuration.LastChannels.Remove(instance.Channel);
            }
            instance.Configuration.LastChannels.Insert(0, instance.Channel);
        }

        #region Event Handlers

        void onChannelVarsChanged(bool isguest)
        {
            if(instance.Client!=null)this.disconnect();
            this.connectUsingInstance();
        }

        #region Events in different thread

        private bool isCapturing = false;

        void onGlobalKeyPressed(object sender, GlobalKeyEventArgs ev)
        {
            if (ev.Key == Key.F2 && this.instance.Client.IsOnChat && this.instance.Configuration.IsHookCaptureEnabled)
            {
                MessageBox.Dispatcher.Invoke(new Action(this.enterOnCaptureMode));
            }
            if (isCapturing)
            {
                if (ev.Key == Key.Left)
                {
                    MessageBox.Dispatcher.Invoke(new Action<bool>(this.moveSelectorOnMessageBox),false);
                }
                else if (ev.Key == Key.Right)
                {
                    MessageBox.Dispatcher.Invoke(new Action<bool>(this.moveSelectorOnMessageBox), true);
                }
            }
        }
        
        void onGlobalKeyUp(object sender, GlobalKeyEventArgs ev)
        {
            if (isCapturing)
            {
                if (ev.Key == Key.Enter)
                {
                    MessageBox.Dispatcher.Invoke(new Action(this.leaveOnCaptureMode));
                }
               if (ev.PrintableKey != "" && ev.PrintableKey!="\b")
                {
                    MessageBox.Dispatcher.Invoke(new Action<String>(insertCharacterOnMessageBox), ev.PrintableKey);
                }
                else if (ev.Key == Key.Back)
                {
                    MessageBox.Dispatcher.Invoke(new Action(removeCharacterOnMessageBox));
                }
            }
        }

        void onLogged(string username)
        {
            this.Dispatcher.Invoke(new Action(regenerateIcons));
            printInfoMessage( username=="guest" ? "LoggedGuestIndication" : "LoggedIndication", new String[] {username});
            this.Dispatcher.Invoke(new Action(syncGuestModeWithClient));
        }

        void onJoinedOnChannel(string channelName)
        {
            printInfoMessage("JoinedIndication", new String[] { channelName });
            if (transitionIrc != null)
            {
                this.Dispatcher.Invoke(new Action(passToUseTrasitionClient));
            }
            this.Dispatcher.Invoke(new Action(syncGuestModeWithClient));
            this.Dispatcher.Invoke(new Action(saveChannelOnConfig));
        }

        void onMuteRequest(User user)
        {
            ChatBox.Dispatcher.BeginInvoke(new Action<User>(muteMessages), user);
        }

        void onInfoMessage(string message, bool isError = false)
        {
            ChatBox.Dispatcher.Invoke(new Action<String, bool>(printInfoMessage), message, isError);
        }

        void onChannelMessageReceived(User owner, string message)
        {
            ChatBox.Dispatcher.BeginInvoke(new Action<User, String>(printMessage), owner, message);
        }

        #endregion
        #region Events of UI

        private void onClose(object sender, EventArgs e)
        {
            if(this.instance.Client!=null) this.instance.Client.Disconnect();
            instance.Configuration.LastUsername = instance.Username;
            instance.Configuration.Password = instance.Password;
            instance.Configuration.Save();
            this.Close();
            //Application.Current.Shutdown();
        }

        private bool inResize = false;
        private Point currentMousePosition;

        private void onResizeEnter(object sender, MouseButtonEventArgs e)
        {
            inResize = true;
            currentMousePosition = e.MouseDevice.GetPosition(this);
            ResizeRect.CaptureMouse();
            ResizeRect.Cursor = Cursors.None;
            Console.WriteLine("Enter");
        }

        private void onResizeOver(object sender, MouseButtonEventArgs e)
        {
            inResize = false;
            ResizeRect.ReleaseMouseCapture();
            ResizeRect.Cursor = Cursors.Arrow;
            Console.WriteLine("Over");
        }

        private void onResizeMove(object sender, MouseEventArgs e)
        {
            if (inResize)
            {
                var move = e.MouseDevice.GetPosition(this) - currentMousePosition;
                currentMousePosition = e.MouseDevice.GetPosition(this);
                if (this.Width + move.X > 0 && this.Height + move.Y > 0)
                {
                    this.Height += move.Y;
                    this.Width += move.X;
                }
                Console.WriteLine(move.ToString());
            }
        }

        private void onMoveEnter(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void onClickMessageBox(object sender, MouseButtonEventArgs e)
        {
            if (guestMode)
            {
                showChannelPicker(true);
            }
        }

        private void onMessageBoxFocused(object sender, RoutedEventArgs e)
        {
            MessageBox.Opacity = 1.0;
            isFocusedOnMessageBox = true;
        }

        private void onMessageBoxUnfocused(object sender, RoutedEventArgs e)
        {
            MessageBox.Opacity = 0.1;
            isFocusedOnMessageBox = false;
        }

        private bool passNextScroll = false;

        private void onScrolledChatBox(object sender, ScrollChangedEventArgs e)
        {
            keepToEnd = Scroller.ScrollableHeight == Scroller.VerticalOffset;
            if (!keepToEnd && !passNextScroll)
            {
                //Scroller.ScrollToVerticalOffset(e.VerticalChange + e.VerticalOffset);
                Console.WriteLine(-1*movedOffset);
                Scroller.ScrollToVerticalOffset(Scroller.VerticalOffset - movedOffset);
                passNextScroll = true;
            }

        }
        
        private void onMessageBoxKeyUp(object sender, KeyEventArgs e)
                {
                    if (e.Key == Key.Enter)
                    {
                        sendMessageFromMessageBox();
                    }
                }

        #endregion

        #endregion

        private void onShowEmoticonsList(object sender, RoutedEventArgs e)
        {
            if (instance.Client.IsOnChat)
            {
                var dialog = new EmoticonsList(instance);
                dialog.Show();
            }
        }

        private void onToggleTranslation(object sender, RoutedEventArgs e)
        {
            if (instance.ShowTranslation)
            {
                ((MenuItem)sender).Header = instance.StringTables["ShowTranslation"];
                instance.ShowTranslation = false;
            }
            else
            {
                ((MenuItem)sender).Header = instance.StringTables["HideTranslation"];
                instance.ShowTranslation = true;
            }
        }

        private void onShowSettings(object sender, RoutedEventArgs e)
        {
            var d=new ConfigurationDialog(instance);
            d.ShowDialog();
        }
    }
}
