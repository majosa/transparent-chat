﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Chat_streamer
{
    class PicturePreviewLink
    {
        private Uri path;
        private Hyperlink block;
        private BitmapDecoder downloader;

        PicturePreviewLink(Uri path, Hyperlink block)
        {
            this.path=path;
            this.block=block;
            this.downloader=BitmapDecoder.Create(path,BitmapCreateOptions.None,BitmapCacheOption.Default);
            this.downloader.DownloadCompleted+=onDownloadComplete;
        }

        void onDownloadComplete(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            Image image=new Image();
            image.Source=downloader.Frames[0];
            image.Stretch=Stretch.Uniform;
            tt.Content=image;
            block.Dispatcher.BeginInvoke(new Action<ToolTip,Hyperlink>(delegate(ToolTip ttS,Hyperlink hS){
                hS.ToolTip = ttS;
            }),new object[]{tt,block});
        }

        public static PicturePreviewLink Create(Uri path, Hyperlink block)
        {
            return new PicturePreviewLink(path, block);
        }
    }
}
