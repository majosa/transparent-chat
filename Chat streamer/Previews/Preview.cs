﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Reflection;

namespace Chat_streamer.Previews
{
    public abstract class Preview
    {
        private static List<Type> listPreviewsModule=new List<Type>();
        public delegate void HyperlinkInfoAvaiableHandle(String newName, ToolTip newTip);
        public event HyperlinkInfoAvaiableHandle HyperlinkInfoAvaiable;

        private static Preview Create(Uri path)
        {
            return null;
        }
        private static bool Check(Uri path)
        {
            return false;
        }
        protected void ThrowHyperlinkInfoAvaiable(String newName, ToolTip newTip){
            HyperlinkInfoAvaiable(newName, newTip);
        }
        public static Preview LookForPreviews(Uri path){
            if (listPreviewsModule.Count<=0)
            {
                foreach (var i in Assembly.GetCallingAssembly().GetTypes())
                {
                    if (i.IsClass && i.IsSubclassOf(typeof(Preview)))
                    {
                        listPreviewsModule.Add(i);
                    }
                }
            }
            foreach (var i in listPreviewsModule)
            {
                var result = (bool)i.InvokeMember("Check", BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public, Type.DefaultBinder, i, new object[] { path });
                if (result) return (Preview)i.InvokeMember("Create", BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public, Type.DefaultBinder, i, new object[] { path });
            }
            return null;
        }
    }
}
