﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Xml.Linq;

namespace Chat_streamer.Previews
{
    public class YoutubeVideoInfo : DependencyObject
    {
        public readonly static DependencyProperty TitleProperty=DependencyProperty.Register("Title",typeof(String),typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());
        public readonly static DependencyProperty AuthorProperty = DependencyProperty.Register("Author", typeof(String), typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());
        public readonly static DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(String), typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());
        public readonly static DependencyProperty ThumbnailProperty = DependencyProperty.Register("Thumbnail", typeof(BitmapFrame), typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());
        public readonly static DependencyProperty DurationProperty = DependencyProperty.Register("Duration", typeof(TimeSpan), typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());
        public readonly static DependencyProperty LikesProperty = DependencyProperty.Register("Likes", typeof(uint), typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());
        public readonly static DependencyProperty DislikesProperty = DependencyProperty.Register("Unlikes", typeof(uint), typeof(YoutubeVideoInfo),
            new FrameworkPropertyMetadata());

        public String Title
        {
            get { return (String)this.GetValue(YoutubeVideoInfo.TitleProperty); }
            set { this.SetValue(YoutubeVideoInfo.TitleProperty, value); }
        }
        public String Author
        {
            get { return (String)this.GetValue(YoutubeVideoInfo.AuthorProperty); }
            set { this.SetValue(YoutubeVideoInfo.AuthorProperty, value); }
        }
        public String Description
        {
            get { return (String)this.GetValue(YoutubeVideoInfo.DescriptionProperty); }
            set { this.SetValue(YoutubeVideoInfo.DescriptionProperty, value); }
        }
        public BitmapFrame Thumbnail
        {
            get { return (BitmapFrame)this.GetValue(YoutubeVideoInfo.ThumbnailProperty); }
            set { this.SetValue(YoutubeVideoInfo.ThumbnailProperty, value); }
        }
        public TimeSpan Duration
        {
            get { return (TimeSpan)this.GetValue(YoutubeVideoInfo.DurationProperty); }
            set { this.SetValue(YoutubeVideoInfo.DurationProperty, value); }
        }
        public uint Likes
        {
            get { return (uint)this.GetValue(YoutubeVideoInfo.LikesProperty); }
            set { this.SetValue(YoutubeVideoInfo.LikesProperty, value); }
        }
        public uint Dislikes
        {
            get { return (uint)this.GetValue(YoutubeVideoInfo.DislikesProperty); }
            set { this.SetValue(YoutubeVideoInfo.DislikesProperty, value); }
        }

    }
    public class YoutubePreviewLink : Preview
    {
        private static YoutubeVideoInfo keep = new YoutubeVideoInfo();
        private YoutubeVideoInfo info ;

        YoutubePreviewLink(Uri path)
        {
            //Get true url
            var vStart = path.Query.IndexOf("v=");
            var vEnd = path.Query.IndexOf("&", vStart + 2);
            String id;
            if (vEnd > 0)
            {
                id = path.Query.Substring(vStart + 2, vEnd - vStart - 2);
            }
            else
            {
                id = path.Query.Substring(vStart + 2);
            }

            var infoUri = new Uri("https://gdata.youtube.com/feeds/api/videos/" + id + "?v=2");
            var clientForInfo = new WebClient();
            clientForInfo.OpenReadCompleted += onInfoOpened;
            clientForInfo.OpenReadAsync(infoUri);
        }

        void onInfoOpened(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Console.WriteLine("Youtube API Error");
                Console.WriteLine(e.Error.Message);
            }
            XDocument doc = XDocument.Load(e.Result);
            XElement entry = doc.Root;
            info = new YoutubeVideoInfo();
            info.Title = entry.Element("{http://www.w3.org/2005/Atom}title").Value;
            info.Author = entry.Element("{http://www.w3.org/2005/Atom}author").Element("{http://www.w3.org/2005/Atom}name").Value;
            var media = entry.Element("{http://search.yahoo.com/mrss/}group");
            var tUri = from i in media.Elements("{http://search.yahoo.com/mrss/}thumbnail") where i.Attribute("{http://gdata.youtube.com/schemas/2007}name").Value == "mqdefault" select i.Attribute("url").Value;
            var thumbnailUri = new Uri(tUri.First());
            info.Duration = TimeSpan.FromSeconds(Double.Parse(media.Element("{http://gdata.youtube.com/schemas/2007}duration").Attribute("seconds").Value));
            info.Description = media.Element("{http://search.yahoo.com/mrss/}description").Value;
            var rating = entry.Element("{http://gdata.youtube.com/schemas/2007}rating");
            info.Dislikes = uint.Parse(rating.Attribute("numDislikes").Value);
            info.Likes = uint.Parse(rating.Attribute("numLikes").Value);
            //Get thumbnail
            DownloaderArgs args = new DownloaderArgs();
            args.path = thumbnailUri;
            args.options = BitmapCreateOptions.None;
            args.cache = BitmapCacheOption.Default;
            args.downloadedHandle = onDownloadComplete;
            DownloadersQueue.AddToQueue(args);
        }

        void onDownloadComplete(object sender, EventArgs e)
        {
            info.Thumbnail = ((BitmapDecoder)sender).Frames[0];
            ToolTip tt = new ToolTip();
            var ypcontrol = new YoutubePreviewToolTip();
            ypcontrol.DataContext = info;
            tt.Content = ypcontrol;
            this.ThrowHyperlinkInfoAvaiable(info.Title, tt);
        }

        public static bool Check(Uri path)
        {
            return path.Host == "www.youtube.com" && path.Query.Contains("v=");
        }

        public static YoutubePreviewLink Create(Uri path)
        {
            return new YoutubePreviewLink(path);
        }
    }
}
