﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Chat_streamer.Previews
{
    public class ImageInfo : DependencyObject
    {
        public readonly static DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(String), typeof(ImageInfo),
            new PropertyMetadata());
        public readonly static DependencyProperty AuthorProperty = DependencyProperty.Register("Author", typeof(String), typeof(ImageInfo),
            new PropertyMetadata());
        public readonly static DependencyProperty CaptionProperty = DependencyProperty.Register("Caption", typeof(String), typeof(ImageInfo),
            new PropertyMetadata());
        public readonly static DependencyProperty ThumbnailProperty = DependencyProperty.Register("Thumbnail", typeof(BitmapFrame), typeof(ImageInfo),
            new PropertyMetadata());
        
        public String Title
        {
            get { return (String)this.GetValue(ImageInfo.TitleProperty); }
            set { this.SetValue(ImageInfo.TitleProperty, value); }
        }
        public String Author
        {
            get { return (String)this.GetValue(ImageInfo.AuthorProperty); }
            set { this.SetValue(ImageInfo.AuthorProperty, value); }
        }
        public String Caption
        {
            get { return (String)this.GetValue(ImageInfo.CaptionProperty); }
            set { this.SetValue(ImageInfo.CaptionProperty, value); }
        }
        public BitmapFrame Thumbnail
        {
            get { return (BitmapFrame)this.GetValue(ImageInfo.ThumbnailProperty); }
            set { this.SetValue(ImageInfo.ThumbnailProperty, value); }
        }

    }
    public class ImgurPreview : Preview
    {
        private static ImageInfo keep = new ImageInfo();
        private ImageInfo info ;
        private WebClient clientForInfo;

        ImgurPreview(Uri path)
        {
            //Get true url
            var vStart = path.AbsolutePath.LastIndexOf("/");
            var vEnd = path.AbsolutePath.LastIndexOf(".");
            String id;
            if (vEnd > 0)
            {
                id = path.AbsolutePath.Substring(vStart + 1, vEnd - vStart - 1);
            }
            else
            {
                id = path.AbsolutePath.Substring(vStart + 1);
            }

            var infoUri = new Uri("https://api.imgur.com/2/image/" + id);
            clientForInfo = new WebClient();
            clientForInfo.OpenReadCompleted += onInfoOpened;
            clientForInfo.OpenReadAsync(infoUri);
        }

        void onInfoOpened(object sender, OpenReadCompletedEventArgs e)
        {
            XDocument doc = XDocument.Load(e.Result);
            XElement image = doc.Root;
            XElement imageS = image.Element("image");
            info = new ImageInfo();
            info.Title = imageS.Element("title").Value;
            //info.Author = entry.Element("{http://www.w3.org/2005/Atom}author").Element("{http://www.w3.org/2005/Atom}name").Value;
            var thumbnailUri = new Uri(image.Element("links").Element("large_thumbnail").Value);
            info.Caption = imageS.Element("caption").Value;
            //Get thumbnail
            DownloaderArgs args = new DownloaderArgs();
            args.path = thumbnailUri;
            args.options = BitmapCreateOptions.None;
            args.cache = BitmapCacheOption.Default;
            args.downloadedHandle = onDownloadComplete;
            DownloadersQueue.AddToQueue(args);
        }

        void onDownloadComplete(object sender, EventArgs e)
        {
            info.Thumbnail = ((BitmapDecoder)sender).Frames[0];
            ToolTip tt = new ToolTip();
            var ypcontrol = new ImagePreviewToolTip();
            ypcontrol.DataContext = info;
            tt.Content = ypcontrol;
            this.ThrowHyperlinkInfoAvaiable("Imgur: "+info.Title, tt);
        }

        public static bool Check(Uri path)
        {
            if (path.Host == "i.imgur.com") return true;
            return path.Host == "imgur.com"&&Regex.IsMatch(path.AbsolutePath, "^/(?<id>[a-zA-Z]+\\d+[a-zA-Z]*|\\d+[a-zA-Z]+\\d*)/?$");
        }

        public static ImgurPreview Create(Uri path)
        {
            return new ImgurPreview(path);
        }
    }
}
