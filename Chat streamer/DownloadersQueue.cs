﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;
using System.Windows.Media.Imaging;

namespace Chat_streamer
{
    public class DownloaderArgs{
        public Uri path;
        public BitmapCreateOptions options;
        public BitmapCacheOption cache;
        public EventHandler downloadedHandle;
        public WebClient client;
        public System.IO.Stream stream;
        public BitmapDecoder decoder;
    }
    static class DownloadersQueue
    {
        private const int MAX_DOWLOADERS = 3;

        private static List<DownloaderArgs> activeDownloaders = new List<DownloaderArgs>();
        private static Dictionary<WebClient, DownloaderArgs> argsForClient = new Dictionary<WebClient, DownloaderArgs>();
        private static Dictionary<BitmapDecoder, DownloaderArgs> argsForDecoder = new Dictionary<BitmapDecoder, DownloaderArgs>();
        private static List<DownloaderArgs> queuedDownloaders = new List<DownloaderArgs>();
        private static int numA;
        private static int numI;
        private static int numR;

        public static void AddToQueue(DownloaderArgs downloaderArgs)
        {
            lock (queuedDownloaders)
            {
                queuedDownloaders.Add(downloaderArgs);
            }
            runQueue();
        }
        private static void runQueue()
        {
            if (queuedDownloaders.Count>0 && activeDownloaders.Count < MAX_DOWLOADERS)
            {
                numR++;
                Console.WriteLine("Request" + numR);
                DownloaderArgs downloaderArgs;
                lock (queuedDownloaders)
                {
                    downloaderArgs = queuedDownloaders.ElementAt(0);
                    queuedDownloaders.RemoveAt(0);
                }
                WebClient client = new WebClient();
                client.OpenReadCompleted+=new OpenReadCompletedEventHandler(delegate(object sender,OpenReadCompletedEventArgs ev){
                    numA++;
                    Console.WriteLine("Async" + numA);
                    if (ev.Error != null)
                    {
                        Console.WriteLine(ev.Error.Message);
                        return;
                    }
                    downloaderArgs.stream = ev.Result;
                    var downloader = BitmapDecoder.Create(ev.Result, downloaderArgs.options, downloaderArgs.cache);
                    downloader.DownloadCompleted += downloadedHandler;
                    downloader.DownloadFailed += onFailToDecode;
                    DownloaderArgs args;
                    lock (argsForClient)
                    {
                        args = argsForClient[(WebClient)sender];
                    }
                    args.decoder = downloader;
                    if (downloaderArgs.downloadedHandle != null) downloader.DownloadCompleted += downloaderArgs.downloadedHandle;
                    lock (argsForDecoder)
                    {
                        argsForDecoder.Add(downloader, args);
                    }
                });
                downloaderArgs.client = client;
                lock (argsForClient)
                {
                    argsForClient.Add(client, downloaderArgs);
                }
                lock(activeDownloaders){
                    activeDownloaders.Add(downloaderArgs);
                    client.OpenReadAsync(downloaderArgs.path);
                }
                runQueue();
            }
        }

        static void onFailToDecode(object sender, System.Windows.Media.ExceptionEventArgs e)
        {
            Console.WriteLine(e.ErrorException.InnerException);
        }
        private static void downloadedHandler(object sender, EventArgs ev)
        {
            numI++;
            Console.WriteLine("Image"+numI);
            lock (activeDownloaders)
            {
                activeDownloaders.Remove(argsForDecoder[(BitmapDecoder)sender]);
            }
            lock (argsForClient)
            {
                argsForClient.Remove(argsForDecoder[(BitmapDecoder)sender].client);
            }
            lock (argsForDecoder)
            {
                argsForDecoder[(BitmapDecoder)sender].stream.Close();
                argsForDecoder.Remove((BitmapDecoder)sender);
            }
            runQueue();
        }
    }
}
