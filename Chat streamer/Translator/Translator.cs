﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Chat_streamer.Translator
{
    public abstract class Translator
    {
        public static String TitleOfProvider { get { return null; } }
        public delegate void DetectedLangCallback(String textOfDetection, Language prediction);
        public delegate void TranslatedTextCallback(String translatedText, Language from, Language to, String originalText);
        protected InstanceRun instance;

        public Translator(InstanceRun ins)
        {
            instance=ins;
        }

        public abstract Language DetectLangForText(String text);
        public abstract String TranslateText(String text, Language from);
        public abstract String TranslateText(String text, Language from, Language to);

        public abstract void DetectLangForText(String text, DetectedLangCallback callback);
        public abstract void TranslateText(String text, Language from, TranslatedTextCallback callback);
        public abstract void TranslateText(String text, Language from, Language to, TranslatedTextCallback callback);
    }

    public static class TranslatorManager
    {
        private static Type defaultTranslator;
        private static List<Type> translatorList;

        static TranslatorManager()
        {
            translatorList = new List<Type>();
            foreach (var i in Assembly.GetCallingAssembly().GetTypes())
            {
                if (i.IsClass && i.IsSubclassOf(typeof(Translator)))
                {
                    translatorList.Add(i);
                }
            }
            if(translatorList.Count>0) defaultTranslator = translatorList[0];
        }

        public static List<String> NamesTranslators
        {
            get
            {
                List<String> r = new List<string>();
                foreach (var tr in translatorList)
                {
                    r.Add((String)tr.GetProperty("TitleOfProvider").GetValue(null,null));
                }
                return r;
            }
        }

        public static String DefaultNameTranslator
        {
            get { return (String)defaultTranslator.GetProperty("TitleOfProvider").GetValue(null,null); }
        }

        public static Translator CreateTranslator(String name, InstanceRun instance)
        {
            foreach (var tr in translatorList)
            {
                if ((String)tr.GetProperty("TitleOfProvider").GetValue(null,null) == name)
                {
                    var constructor = tr.GetConstructor(new Type[] { typeof(InstanceRun) });
                    var translator = (Translator)constructor.Invoke(new object[] { instance });
                    return translator;
                }
            }
            return null;
        }
    }
}
