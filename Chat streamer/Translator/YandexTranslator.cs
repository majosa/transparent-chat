﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace Chat_streamer.Translator
{
    public class YandexTranslator : Translator
    {

        public static string TitleOfProvider
        {
            get { return "Yandex"; }
        }

        public override Language DetectLangForText(string text)
        {
            Thread t = Thread.CurrentThread;
            Language r=null;
            this.DetectLangForText(text, new DetectedLangCallback(delegate(String textO, Language prediction)
            {
                r = prediction;
                t.Interrupt();
            }));
            Thread.CurrentThread.Join();
            return r;
        }

        public YandexTranslator(InstanceRun ins) : base(ins)
        {

        }

        public override string TranslateText(string text, Language from)
        {
            throw new NotImplementedException();
        }

        public override string TranslateText(string text, Language from, Language to)
        {
            throw new NotImplementedException();
        }

        public override void DetectLangForText(string text, Translator.DetectedLangCallback callback)
        {
            var connection = new WebClient();
            connection.Headers.Add("Referer", "http://translate.yandex.ru/");
            connection.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36");
            connection.OpenReadAsync(new Uri("http://translate.yandex.ru/api/v1/tr/detect?text="+text));
            connection.OpenReadCompleted += delegate(object sender, OpenReadCompletedEventArgs e)
            {
                XDocument doc=XDocument.Load(e.Result);
                if (doc.Root.Attribute("code").Value == "200")
                {
                    callback(text, LanguageManager.getLanguage(doc.Root.Attribute("lang").Value));
                }
            };
        }

        public override void TranslateText(string text, Language from, Translator.TranslatedTextCallback callback)
        {
            this.TranslateText(text, from, instance.Configuration.Language, callback);
        }

        public override void TranslateText(string text, Language from, Language to, Translator.TranslatedTextCallback callback)
        {
            var connection = new WebClient();
            connection.Headers.Add("Referer", "http://translate.yandex.ru/");
            connection.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36");
            connection.OpenReadAsync(new Uri("http://translate.yandex.net/api/v1/tr/translate?lang=" +
                from.CulturalInfo.TwoLetterISOLanguageName + "-" + to.CulturalInfo.TwoLetterISOLanguageName + "&text=" + text));
            connection.OpenReadCompleted += delegate(object sender, OpenReadCompletedEventArgs e)
            {
                XDocument doc = XDocument.Load(e.Result);
                if (doc.Root.Attribute("code").Value == "200")
                {
                    callback(doc.Root.Element("text").Value, from, to, text);
                }
            };
        }
    }
}
