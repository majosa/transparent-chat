﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Navigation;
using System.Diagnostics;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using Chat_streamer.Previews;
using Chat_streamer.ChatServices;
using System.ComponentModel;

namespace Chat_streamer
{
    public class MessageParagraph:Paragraph
    {
        private static Regex uriRegex=new Regex("[a-z]+://.[^\\s]+");
        private static Regex urlRegex=new Regex("\\w+\\.[a-z]{2,4}[^\\s]*");
        private User owner;
        private InstanceRun instance;
        private Span messageSpan;
        private Run ownerRun;

        private delegate object RegexCaptureHandle(String text);
        
        public MessageParagraph(InstanceRun ins, User owner,String message)
            : base()
        {
            this.owner = owner;
            this.instance = ins;
            this.Margin = new Thickness(0, 5, 0, 0);
            createStructure(message);
        }

        private void createStructure(String message)
        {
            if (owner.ColorText == new Color()) owner.ColorText = Colors.Black;
            this.Inlines.Add(createHeader());
            this.Inlines.Add(createBody(message));
        }

        private Inline createHeader()
        {
            var header = new Floater();
            var blockForHeader = new Paragraph();
            ownerRun=new Run(owner.UserName);
            ownerRun.FontWeight=FontWeights.SemiBold;
            ownerRun.BaselineAlignment = BaselineAlignment.Center;
            blockForHeader.Inlines.Add(ownerRun);

            foreach (var i in owner.Modes)
            {
                var icon=instance.Badges.GetBadge(i);
                if (icon != null)
                {
                    Image mod = new Image();
                    mod.Source = icon;
                    mod.Stretch = Stretch.None;
                    mod.ToolTip = i.Substring(0, 1).ToUpper() + i.Substring(1);
                    var effect = new DropShadowEffect();
                    effect.BlurRadius = 0.1;
                    effect.ShadowDepth = 0.2;
                    effect.Color = instance.Configuration.TextColor;
                    mod.Effect = effect;
                    blockForHeader.Inlines.Add(new InlineUIContainer(mod));
                }
            }
            header.Blocks.Add(blockForHeader);
            return header;
        }

        private void animateAppearing()
        {
            //Create brushes
            var backgroundBrush = new SolidColorBrush();
            var foregroundBrush = new SolidColorBrush(instance.Configuration.TextColor);
            var ownerBrush = new SolidColorBrush(owner.ColorText);
            //Establish brushes to be animated
            this.Background = backgroundBrush;
            ownerRun.Foreground = ownerBrush;
            this.Foreground = foregroundBrush;
            //Prepare the timelines
            var backgroundTimeline = new ColorAnimationUsingKeyFrames();
            var foregroundTimeline = new ColorAnimationUsingKeyFrames();
            var ownerTimeline = new ColorAnimationUsingKeyFrames();
            //Insert KeyFrames
            backgroundTimeline.KeyFrames.Add(new LinearColorKeyFrame(owner.ColorText, TimeSpan.FromSeconds(0)));
            backgroundTimeline.KeyFrames.Add(new LinearColorKeyFrame(owner.ColorText,TimeSpan.FromSeconds(1)));
            backgroundTimeline.KeyFrames.Add(new LinearColorKeyFrame(backgroundBrush.Color, TimeSpan.FromSeconds(1.5)));
            foregroundTimeline.KeyFrames.Add(new LinearColorKeyFrame(Colors.White, TimeSpan.FromSeconds(0)));
            foregroundTimeline.KeyFrames.Add(new LinearColorKeyFrame(Colors.White, TimeSpan.FromSeconds(1)));
            foregroundTimeline.KeyFrames.Add(new LinearColorKeyFrame(instance.Configuration.TextColor, TimeSpan.FromSeconds(1.25)));
            ownerTimeline.KeyFrames.Add(new LinearColorKeyFrame(Colors.White, TimeSpan.FromSeconds(0)));
            ownerTimeline.KeyFrames.Add(new LinearColorKeyFrame(Colors.White, TimeSpan.FromSeconds(1)));
            ownerTimeline.KeyFrames.Add(new LinearColorKeyFrame(owner.ColorText, TimeSpan.FromSeconds(1.25)));
            //Start animations
            backgroundBrush.BeginAnimation(SolidColorBrush.ColorProperty, backgroundTimeline);
            foregroundBrush.BeginAnimation(SolidColorBrush.ColorProperty, foregroundTimeline);
            ownerBrush.BeginAnimation(SolidColorBrush.ColorProperty, ownerTimeline);
        }

        private Inline createBody(String message)
        {
            messageSpan=new Span();
            this.AddText(message);
            return messageSpan;
        }

        //Our image sources can't be serialized when WPF tries to remove him (WTF) so we must remove it before this element can be deleted
        public void PrepareToBeDeleted(InlineCollection container=null)
        {
            if (container == null)
            {
                container = this.Inlines;
            }
            foreach (var i in container)
            {
                if (i is InlineUIContainer)
                {
                    if (((InlineUIContainer)i).Child is Image)
                    {
                        ((Image)((InlineUIContainer)i).Child).Source = null;
                    }
                }
                if (i is Span)
                {
                    PrepareToBeDeleted(((Span)i).Inlines);
                }
                if (i is AnchoredBlock)
                {
                    foreach (var z in ((AnchoredBlock)i).Blocks)
                    {
                        if (z is Paragraph) this.PrepareToBeDeleted(((Paragraph)z).Inlines);
                    }
                }
            }
        }

        public void EraseMessage()
        {
            var brush = (SolidColorBrush)messageSpan.Foreground;
            brush.BeginAnimation(SolidColorBrush.ColorProperty, new ColorAnimation(new Color(), TimeSpan.FromSeconds(1.0)));
            foreach (var i in messageSpan.Inlines)
            {
                if (i is InlineUIContainer)
                {
                    if (((InlineUIContainer)i).Child is Image)
                    {
                        ((Image)((InlineUIContainer)i).Child).BeginAnimation(Image.OpacityProperty, new DoubleAnimation(0.0, TimeSpan.FromSeconds(1.0)));
                    }
                }
                if (i is Hyperlink)
                {
                    ((Hyperlink)i).Foreground=((Hyperlink)i).Foreground.Clone();
                    ((Hyperlink)i).Foreground.BeginAnimation(SolidColorBrush.ColorProperty, new ColorAnimation(new Color(), TimeSpan.FromSeconds(1.0)));
                    ((Hyperlink)i).IsEnabled = false;
                }
            }
        }

        public void AddText(String moreMessage)
        {
            List<object> structuredM = new List<object>();
            structuredM.Add(moreMessage);
            structuredM = reviewStructure(structuredM, uriRegex, createHyperlink);
            structuredM = reviewStructure(structuredM, urlRegex, createWeakHyperlink);
            foreach (KeyValuePair<Regex,Emoticon> emo in instance.Emoticons.GetList(owner.EmoticonsSetsAllowed))
            {
                structuredM = reviewStructure(structuredM, emo.Key, new RegexCaptureHandle(delegate(String text)
                {
                    return createEmoticonImage(emo.Value);
                }));
            }
            foreach (object part in structuredM)
            {
                if (part == null) continue;
                Inline ob;
                ob = part is String ? new TranslableTextRun(instance, (String)part) : (Inline)part;
                messageSpan.Inlines.Add(ob);
            }
            animateAppearing();
        }

        private List<Object> reviewStructure(List<Object> structure,Regex regex,RegexCaptureHandle handler)
        {
            List<Object> nstruct = new List<object>();
            foreach (var part in structure)
            {
                if (part is String)
                {
                    String textpart = (String)part;
                    String[] cleanparts = regex.Split(textpart);
                    int numrest = 0;
                    if (cleanparts[numrest] != null || cleanparts[numrest] != "")
                    {
                        nstruct.Add(cleanparts[numrest]);
                    }
                    foreach (Match match in regex.Matches(textpart))
                    {
                        numrest++;
                        nstruct.Add(handler(match.Groups[0].Value));
                        if (cleanparts[numrest] != null || cleanparts[numrest] != "")
                        {
                            nstruct.Add(cleanparts[numrest]);
                        }
                    }
                }
                else
                {
                    nstruct.Add(part);
                }
            }
            return nstruct;
        }

        private Hyperlink createHyperlink(String urit)
        {
            //filters
            Hyperlink link = new Hyperlink();
            Uri uri;
            try
            {
                uri = new Uri(urit);
            }
            catch (System.UriFormatException ex)
            {
                return null;
            }
            link.Inlines.Add(urit);
            link.NavigateUri = uri;
            //previews
            var preview = Preview.LookForPreviews(uri);
            if (preview != null)
            {
                preview.HyperlinkInfoAvaiable += new Preview.HyperlinkInfoAvaiableHandle(
                    delegate(String newName, ToolTip newTip)
                    {
                        link.Dispatcher.BeginInvoke(new Action(delegate()
                        {
                            if (newName != null && newName != "")
                            {
                                link.Inlines.Clear();
                                link.Inlines.Add(newName);
                            }
                            link.ToolTip = newTip;
                        }));
                    }
                );
            }
            link.RequestNavigate+=new RequestNavigateEventHandler(delegate(object sender,RequestNavigateEventArgs ev){
                Process.Start(ev.Uri.ToString());
                ev.Handled = true;
            });
            return link;
        }

        private Hyperlink createWeakHyperlink(string urlt)
        {
            return createHyperlink("http://" + urlt);
        }

        private InlineUIContainer createEmoticonImage(Emoticon emoticon)
        {
            Image emoticonImage = new Image();
            emoticonImage.Source = emoticon.Image;
            emoticonImage.Stretch = Stretch.Uniform;
            emoticonImage.Height = emoticon.Size.Height;
            emoticonImage.Width = emoticon.Size.Width;
            return new InlineUIContainer(emoticonImage);
        }

        private bool checkUrl(String url)
        {
            try
            {
                var uri = new Uri(url);
                String[] protocols = { "http", "https", "steam", "skype" };
                return protocols.Contains(uri.Scheme);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return false;

        }


        public String GetOwner()
        {
            return owner.UserName;
        }

        /*private void PrintMessage(String owner, Color ownerColor, String message)
        {
            Paragraph messageBlock = new Paragraph();
            //Handler for badges
            BitmapSource icon = null;

            switch (irc.GetUserMode(owner))
            {
                case TwitchUserMode.Mod:
                    icon = badges.Mod;
                    break;
                case TwitchUserMode.Subscriptor:
                    icon = badges.Subscriptor;
                    break;
                case TwitchUserMode.Boardcaster:
                    icon = badges.Boardcaster;
                    break;
            }
            if (icon != null)
            {
                Image mod = new Image();
                mod.Source = icon;
                mod.Stretch = Stretch.None;
                var effect = new DropShadowEffect();
                effect.BlurRadius = 0.1;
                effect.Color = settings.TextColor;
                mod.Effect = effect;
                messageBlock.Inlines.Add(new InlineUIContainer(mod));
            }
            //owner
            Run ownerBlock = new Run("<" + owner + ">");
            ownerBlock.FontWeight = FontWeights.Bold;
            ownerBlock.Foreground = new SolidColorBrush(ownerColor);
            messageBlock.Inlines.Add(ownerBlock);
            //message
            Span messageSpan = new Span();
            foreach (var i in message.Split(' '))
            {
                if (i.Contains("://") && checkUrl(i))
                {
                    //filters
                    Hyperlink link = new Hyperlink();
                    Uri uri = new Uri(i);
                    link.Inlines.Add(i);
                    link.MouseLeftButtonDown += linkClicked;
                    link.Cursor = Cursors.Hand;
                    link.Tag = uri;
                    //preview
                    String[] imagesExt = { "jpg", "png", "gif" };
                    var pointSep = uri.LocalPath.LastIndexOf('.');
                    if (pointSep > 0)
                    {
                        var fileType = uri.LocalPath.Substring(pointSep + 1);
                        if (imagesExt.Contains(fileType) && settings.IsEnabledPreviews)
                        {
                            PicturePreviewLink.Create(new Uri(i), link);
                            link.MouseEnter += onUpdateToolTip;
                            link.MouseLeave += onDesactivatePreview;
                        }
                    }
                    if (YoutubePreviewLink.Check(uri) && settings.IsEnabledPreviews)
                    {
                        YoutubePreviewLink.Create(new Uri(i), link);
                        link.MouseEnter += onUpdateToolTip;
                        link.MouseLeave += onDesactivatePreview;
                    }
                    messageSpan.Inlines.Add(" ");
                    messageSpan.Inlines.Add(link);
                }
                if (i.Length>7 && i.Substring(0, 7) == "http://")
                {
                    Hyperlink link = new Hyperlink();
                    //link.Foreground = new SolidColorBrush(Colors.Blue);
                    //link.TextDecorations.Add(TextDecorations.Underline);
                    //link.Cursor = Cursors.Hand;
                    //link.MouseLeftButtonDown += linkClicked;
                    messageSpan.Inlines.Add(link);
                }
                else
                {
                    //emoticons
                    bool emoticonInserted = false;
                    foreach (var emoticon in emoticons.EmoticonsList)
                    {
                        if (emoticon.Key.IsMatch(i))
                        {
                            Image emoticonImage = new Image();
                            emoticonImage.Source = emoticon.Value.Image;
                            emoticonImage.Stretch = Stretch.None;
                            messageSpan.Inlines.Add(" ");
                            messageSpan.Inlines.Add(new InlineUIContainer(emoticonImage));
                            emoticonInserted = true;
                            break;
                        }
                    }
                    if (!emoticonInserted)
                    {
                        messageSpan.Inlines.Add(" " + i);
                    }
                }
            }
            messageBlock.Inlines.Add(messageSpan);
            ChatBox.Document.Blocks.Add(messageBlock);
            ChatBox.ScrollToEnd();
        }
        */
        private class TranslableTextRun : Run
        {
            private InstanceRun instance;
            private String originalText;
            private Language originLang;
            private Language translatedLang;
            private String translation;

            public TranslableTextRun(InstanceRun ins, String text)
                : base(text)
            {
                instance = ins;
                originalText = text;
                DependencyPropertyDescriptor.FromProperty(InstanceRun.ShowTranslationProperty, typeof(InstanceRun)).AddValueChanged(instance, new EventHandler(delegate(Object sender, EventArgs ev)
                {
                    if (instance.ShowTranslation)
                    {
                        this.Text = translation;
                    }
                    else
                    {
                        this.Text = originalText;
                    }
                }));
                DependencyPropertyDescriptor.FromProperty(Configuration.LanguageProperty, typeof(Configuration)).AddValueChanged(instance.Configuration, new EventHandler(delegate(Object sender, EventArgs ev)
                {
                    if (originLang == null) detectLang(); else translate();
                }));
                if (ins.Configuration.IsTranslationEnabled) detectLang();
            }

            private void detectLang()
            {
                var tr = Translator.TranslatorManager.CreateTranslator(instance.Configuration.PreferedTranslatorModule, instance);
                tr.DetectLangForText(Text, new Translator.Translator.DetectedLangCallback(delegate(String text, Language prediction)
                {
                    if (prediction != instance.Configuration.Language && prediction != null)
                    {
                        originLang = prediction;
                        if(instance.Configuration.IsTranslationEnabled) this.translate();
                    }
                }));
            }

            private void translate()
            {
                if (originLang == instance.Configuration.Language)
                {
                    translatedLang = originLang;
                    translation = originalText;
                }
                else if (translatedLang != instance.Configuration.Language)
                {
                    var tr = Translator.TranslatorManager.CreateTranslator(instance.Configuration.PreferedTranslatorModule, instance);
                    tr.TranslateText(originalText, originLang, new Translator.Translator.TranslatedTextCallback(onTraslatedText));
                }
            }

            private void onTraslatedText(String translatedText, Language from, Language to, String originalText)
            {
                //this.messageSpan.ToolTip = translatedText;
                translation = translatedText;
                translatedLang = to;
                if(instance.ShowTranslation) this.Text = translatedText;
            }
        }
    }
}
