﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.ComponentModel;
using System.Windows;

namespace Chat_streamer
{
    public class Language
    {
        private CultureInfo cultureInfo;
        private List<Language> alternativeLangs;
        private ResourceDictionary dictionary;
        public Language(CultureInfo ci, ResourceDictionary res, List<Language> aL=null)
        {
            cultureInfo = ci;
            dictionary = res;
            if (aL!=null)
            {
                alternativeLangs = aL;
            }
            else
            {
                alternativeLangs = new List<Language>();
            }
        }

        public CultureInfo CulturalInfo
        {
            get { return cultureInfo; }
        }
        public List<Language> AlternativeLangs
        {
            get { return alternativeLangs; }
        }
        public ResourceDictionary Resources
        {
            get { return dictionary; }
        }
    }
    public class DictionaryLanguage : ResourceDictionary
    {
        private InstanceRun instance;
        public DictionaryLanguage(InstanceRun ins) : base()
        {
            instance=ins;
            DependencyPropertyDescriptor.FromProperty(Configuration.LanguageProperty,typeof(Configuration)).AddValueChanged(ins.Configuration, new EventHandler(delegate(Object ob,EventArgs ev)
                {
                    this.MergedDictionaries.Clear();
                    this.MergedDictionaries.Add(((Configuration)ob).Language.Resources);
                })
            );
            this.MergedDictionaries.Add(ins.Configuration.Language.Resources);
        }

    }
    public static class LanguageManager
    {
        private static Language defaultLang;
        private static List<Language> languages;
        static LanguageManager()
        {
            languages = new List<Language>();
            foreach (CultureInfo c in CultureInfo.GetCultures(CultureTypes.NeutralCultures))
            {
                try
                {
                    if (!UriParser.IsKnownScheme("pack")) new Application();
                    ResourceDictionary resource = new ResourceDictionary{
                        Source = new Uri("./Languages/Language" + c.TwoLetterISOLanguageName.ToUpper() + ".xaml", UriKind.Relative)
                    };
                    Language lang = new Language(c,resource);
                    languages.Add(lang);
                    if (CultureInfo.CurrentCulture.TwoLetterISOLanguageName == c.TwoLetterISOLanguageName)
                    {
                        defaultLang = lang;
                    }
                    else
                    {
                        defaultLang = getLanguage("en");
                    }
                }catch(System.IO.IOException ex){
                    Console.WriteLine(c.TwoLetterISOLanguageName + " don't exist");
                }
            }
        }

        public static List<String> LanguagesListUsingDisplayName
        {
            get
            {
                List<String> r = new List<string>();
                foreach (var l in LanguageManager.languages)
                {
                    r.Add(l.CulturalInfo.NativeName);
                }
                return r;
            }
        }

        public static Language DefaultLanguage
        {
            get { return defaultLang; }
        }

        public static Language getLanguage(String langcode)
        {
            foreach (Language l in languages)
            {
                if (l.CulturalInfo.TwoLetterISOLanguageName == langcode)
                {
                    return l;
                }
            }
            return null;
        }

        public static Language getLanguageByDisplayName(String displayName)
        {
            foreach (Language l in languages)
            {
                if (l.CulturalInfo.NativeName == displayName)
                {
                    return l;
                }
            }
            return null;
        }

    }
}
