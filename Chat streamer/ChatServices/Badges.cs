﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Net;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;

namespace Chat_streamer.ChatServices
{
    public abstract class Badges
    {
        enum BadgeType
        {
            Mod, Admin, Boardcaster, Staff, Subscriptor
        }
        protected Dictionary<String, BitmapSource> badges;
        public Badges(String channel)
        {
            badges = new Dictionary<string, BitmapSource>();
        }

        public virtual BitmapSource GetBadge(String name)
        {
            return badges.ContainsKey(name.ToLower()) ? badges[name.ToLower()] : null;
        }
    }
}
