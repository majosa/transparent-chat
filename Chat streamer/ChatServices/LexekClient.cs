﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using WebSocket4Net;
using Newtonsoft.Json.Linq;
using System.Timers;
using System.Windows.Media;
using System.Windows;

namespace Chat_streamer.ChatServices
{

    public class LexekClient : Client
    {
        private WebSocket connection;
        private List<User> users;

        public static string ServiceName
        {
            get
            {
                return "Lexek chat protocol (Yoba.vg)";
            }
        }

        private Dictionary<String, Color> colorsPalette = new Dictionary<string, Color>
        {
            {"red",Color.FromRgb(255, 0, 0)},
            {"blue",Color.FromRgb(0, 0, 255)},
            {"green",Color.FromRgb(0, 128, 0)},
            {"firebrick",Color.FromRgb(178, 34, 34)},
            {"coral",Color.FromRgb(255, 127, 80)},
            {"yellowgreen",Color.FromRgb(154, 205, 80)},
            {"orangered",Color.FromRgb(255, 69, 0)},
            {"seagreen",Color.FromRgb(37, 139, 87)},
            {"goldenrod",Color.FromRgb(218, 165, 32)},
            {"chocolate", Color.FromRgb(210, 105, 30)},
            {"cadetblue",Color.FromRgb(95, 158, 160)},
            {"dodgerblue",Color.FromRgb(30, 144, 255)},
            {"hotpink",Color.FromRgb(255, 105, 180)},
            {"blueviolet",Color.FromRgb(138, 43, 226)},
            {"springgreen",Color.FromRgb(0, 255, 127)},
            {"gray",Color.FromRgb(128, 128, 128)},
            {"darkred",Color.FromRgb(139, 0, 0)},
            {"midnightblue",Color.FromRgb(25, 25, 112)},
            {"deeppink",Color.FromRgb(255, 14, 147)},
            {"black",Color.FromRgb(0,0,0)}
        };

        public LexekClient(String channel,String username=null, String password="blah") : base(channel,username,password)
        {
            connection = new WebSocket("ws://www.yoba.vg:1488", origin: "http://www.yoba.vg:8080");
            connection.Open();
            connection.Opened += onOpened;
            connection.MessageReceived += onMessage;
            connection.Error+=onError;
            //Login
            if (username!=null)
            {
                var reqLogin = HttpWebRequest.Create("http://www.yoba.vg:8080/login");
                reqLogin.Method = "POST";
                reqLogin.Headers.Add("Origin","http://www.yoba.vg:8080");
                //reqLogin.Headers.Add("Referer", "http://www.yoba.vg:8080/chat.html");
                byte[] encodedReq = new UTF8Encoding().GetBytes("username="+username+"&password="+password);
                reqLogin.ContentLength = encodedReq.Length;
                reqLogin.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
                var strLogin = reqLogin.GetRequestStream();
                strLogin.Write(encodedReq, 0, encodedReq.Length);
                strLogin.Close();
                reqLogin.BeginGetResponse(new AsyncCallback(onLoggedResult), reqLogin);

            }
        }

        private void onLoggedResult(IAsyncResult result)
        {
            var response = ((HttpWebRequest)result.AsyncState).GetResponse();
            String cookie = response.Headers["Set-Cookie"];
            int sidSP=cookie.IndexOf("sid=");
            int sidEP = cookie.IndexOf(';', sidSP + 1);
            var sid = cookie.Substring(sidSP + 4, sidEP - (sidSP + 4));
            this.Dispatcher.BeginInvoke(new Action<String>(delegate(String sidP){
                JObject sessionO = new JObject();
                sessionO.Add("type", new JValue("SESSION"));
                sessionO.Add("args", new JArray(new JValue(sidP)));
                connection.Send(sessionO.ToString());
            }),sid);
        }

        void onMessage(object sender, MessageReceivedEventArgs e)
        {
            JObject message = JObject.Parse(e.Message);
            String tMessage=(String)((JValue)message["type"]).Value;
            if (tMessage == "HIST")
            {

            }
            else if (tMessage == "HALLO")
            {
                if(username==null) raiseLogged("guest");
            }
            else if (tMessage == "LOGIN")
            {
                raiseLogged(username);
            }
            else if (tMessage == "NAMES")
            {
                users.Clear();
                foreach (JObject user in message["args"])
                {
                    users.Add(parseUser(user));
                }
            }
            else if (tMessage == "JOIN")
            {
                users.Add(parseUser((JArray)message["args"]));
            }
            else if (tMessage == "PART")
            {
                foreach (JValue nickname in message["args"])
                {
                    removeUser((String)nickname.Value);
                }
            }
            else if (tMessage == "PONG")
            {
                base.pongReceived = true;
            }
            else if (tMessage == "MSG")
            {
                User owner = GetUser((String)((JValue)((JArray)message["args"])[0]).Value);
                if (owner.UserName == username) return;
                owner.ColorText = parseColor((String)((JValue)((JArray)message["args"])[1]).Value);
                String msg = (String)((JValue)((JArray)message["args"])[2]).Value;
                //3: ID
                //4: Time
                raiseChannelMessageReceived(owner, msg);
            }
        }

        void onOpened(object sender, EventArgs e)
        {
            users = new List<User>();
            raiseConnected();
        }


        User parseUser(JObject userO)
        {
            User r = new User();
            r.UserName = (String)((JValue)userO["name"]).Value;
            r.Modes.Add((String)((JValue)userO["authLevel"]).Value);
            r.ColorText = parseColor((String)((JValue)userO["color"]).Value);
            return r;
        }

        Color parseColor(String str)
        {
            if (str.ElementAt(0) == '#')
            {
                return (Color)ColorConverter.ConvertFromString(str);
            }
            else
            {
                return colorsPalette[str];
            }
        }

        User parseUser(JArray userO)
        {
            User r = new User();
            r.UserName = (String)((JValue)userO[0]).Value;
            r.Modes.Add((String)((JValue)userO[1]).Value);
            /*if (((String)((JValue)userO[2]).Value).ElementAt(0) == '#')
            {
                r.ColorText = (Color)ColorConverter.ConvertFromString((String)((JValue)userO[2]).Value);
            }
            else
            {
                r.ColorText = colorsPalette[(String)((JValue)userO[2]).Value];
            }*/

            return r;
        }

        public override bool IsOnChat
        {
            get { return connection.State == WebSocketState.Open; }
        }

        public override bool IsGuest
        {
            get { return username == null; }
        }

        public override void SendMessageToChannel(String message)
        {
            JObject msg = new JObject();
            msg.Add("type", new JValue("MSG"));
            msg.Add("args", new JArray(new JValue(message)));
            connection.Send(msg.ToString());
        }

        public override void Disconnect()
        {
            IsConnected = false;
            /*connection.Disconnect();
            channel = null;
            channelName = null;*/
        }

        public override User GetUser(String username)
        {
            foreach (var i in users)
            {
                if (i.UserName == username)
                {
                    return i;
                }
            }
            return null;
        }

        private void removeUser(String username)
        {
            var user = GetUser(username);
            if (user != null)
            {
                users.Remove(user);
            }
            else
            {
                Console.WriteLine("User dissapeared: " + username + " wasn't on the list when we tried remove it");
            }
        }

        protected override void sendPing()
        {
            JObject msg = new JObject();
            msg.Add("type", new JValue("PING"));
            msg.Add("args", new JArray());
            connection.Send(msg.ToString());
        }

        protected override void reconnect()
        {
            connection.Close();
            connection.Open();
        }

        void onError(object sender, SuperSocket.ClientEngine.ErrorEventArgs e)
        {
            if (e.Exception.StackTrace.Contains("Chat_streamer"))
            {
                //It's bug from app. Print it
                Console.WriteLine(e.Exception.Message);
                Console.WriteLine(e.Exception.StackTrace);
                //raiseInfoMessageReceived("Internal error, please send run this application from cmd and send the log to the developer.\n" + e.ToString(), true);
            }
            else
            {
                Console.WriteLine("WebSocket4Net Library error");
                Console.WriteLine(e.Exception.Message);
                Console.WriteLine(e.Exception.StackTrace);
            }
        }
    }
}
