﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Windows;

namespace Chat_streamer.ChatServices
{
    public class LexekEmoticons : Emoticons
    {
        public LexekEmoticons(String channel) : base(channel)
        {
            WebClient request = new WebClient();
            request.OpenReadCompleted+=onFileOpened;
            request.OpenReadAsync(new Uri("https://api.twitch.tv/kraken/chat/emoticons"));
            emoticonSets = new List<EmoticonSet>();
        }

        void openResource(Emoticon emoticon)
        {
            DownloaderArgs args = new DownloaderArgs();
            args.path = emoticon.Path;
            args.options = BitmapCreateOptions.None;
            args.cache = BitmapCacheOption.OnDemand;
            args.downloadedHandle = delegate(object sender, EventArgs ev)
            {
                emoticon.Image = ((BitmapDecoder)sender).Frames[0];
            };
            DownloadersQueue.AddToQueue(args);
        }

        void onFileOpened(object sender, OpenReadCompletedEventArgs e)
        {
            JObject doc = (JObject)JObject.Load(new JsonTextReader(new StreamReader(e.Result)));
            JArray emoticonsListE = (JArray)doc["emoticons"];
            Console.WriteLine(emoticonsListE.Children());
            var z = 0;
            foreach(var elementSetE in emoticonsListE){
                var emoticonSet = new EmoticonSet();
                String regexT = (String)((JValue)elementSetE["regex"]).Value;
                foreach (var elementE in elementSetE["images"])
                {
                    var emoticon = new Emoticon();
                    object set = ((JValue)elementE["emoticon_set"]).Value;
                    emoticon.idSet = (set == null) ? (int?)null : (int)((long)set);
                    //emoticon.idSet=(int?)((JValue)elementE["emoticon_set"]).Value;
                    emoticon.Path = new Uri((String)((JValue)elementE["url"]).Value);
                    emoticon.Size = new Size();
                    emoticon.Size.Height = (long)((JValue)elementE["height"]).Value;
                    emoticon.Size.Width = (long)((JValue)elementE["width"]).Value;
                    openResource(emoticon);
                    emoticonSet.Emoticons.Add(emoticon);
                }
                //regexT=regexT.Replace("[", "(?:");
                regexT = regexT.Replace("\\&", "&");
                regexT = regexT.Replace("\\;", ";");
                regexT = regexT.Replace("\\3", "3");
                //regexT = regexT.Replace("&amp;", "&");
                regexT = regexT.Replace("&amp;", "&");
                regexT = regexT.Replace("&lt;", "<");
                regexT = regexT.Replace("&gt;", ">");
                //regexT=regexT.Replace(']', ')');
                if ("\\s" == regexT.Substring(0, 2)) regexT = regexT.Substring(2);
                if ("\\s" == regexT.Substring(regexT.Count() - 2)) regexT = regexT.Substring(0, regexT.Count() - 2);
                emoticonSet.Regex = new Regex(regexT);
                //Colission detector regex
                bool inserted = false;
                for (int i=0;i<emoticonSets.Count;i++)
                {
                    if (emoticonSets[i].Regex.IsMatch(emoticonSet.Regex.ToString()))
                    {
                        emoticonSets.Insert(i, emoticonSet);
                        inserted = true;
                        break;
                    }
                }
                if(!inserted) emoticonSets.Add(emoticonSet);
                z++;
            }
        }
    }
}
