﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Media;
using IrcDotNet;
using System.Windows;

namespace Chat_streamer.ChatServices
{
    public enum TwitchUserMode
    {
        User, Mod, Admin, Boardcaster, Staff, Subscriptor
    }

    public class TwitchClient : Client
    {
        private IrcUserRegistrationInfo identity;
        private IrcClient connection;
        private IrcChannel channel;
        private List<User> users;

        private Dictionary<String, Color> colorsPalette = new Dictionary<string, Color>
        {
            {"red",Color.FromRgb(255, 0, 0)},
            {"blue",Color.FromRgb(0, 0, 255)},
            {"green",Color.FromRgb(0, 128, 0)},
            {"firebrick",Color.FromRgb(178, 34, 34)},
            {"coral",Color.FromRgb(255, 127, 80)},
            {"yellowgreen",Color.FromRgb(154, 205, 80)},
            {"orangered",Color.FromRgb(255, 69, 0)},
            {"seagreen",Color.FromRgb(37, 139, 87)},
            {"goldenrod",Color.FromRgb(218, 165, 32)},
            {"chocolate", Color.FromRgb(210, 105, 30)},
            {"cadetblue",Color.FromRgb(95, 158, 160)},
            {"dodgerblue",Color.FromRgb(30, 144, 255)},
            {"hotpink",Color.FromRgb(255, 105, 180)},
            {"blueviolet",Color.FromRgb(138, 43, 226)},
            {"springgreen",Color.FromRgb(0, 255, 127)},
            {"gray",Color.FromRgb(128, 128, 128)},
            {"darkred",Color.FromRgb(139, 0, 0)},
            {"midnightblue",Color.FromRgb(25, 25, 112)},
            {"deeppink",Color.FromRgb(255, 14, 147)},
            {"black",Color.FromRgb(0,0,0)}
        };

        private List<String> nonProColors = new List<string> {
            "red",
            "blue",
            "green",
            "firebrick",
            "coral",
            "yellowgreen",
            "orangered",
            "seagreen",
            "goldenrod",
            "chocolate",
            "cadetblue",
            "dodgerblue",
            "hotpink",
            "blueviolet",
            "springgreen"
        };

        public static string ServiceName
        {
            get
            {
                return "Twitch IRC chat";
            }
        }

        public static bool IsRequieredOAuth
        {
            get
            {
                return true;
            }
        }

        public static String OAuthPath
        {
            get
            {
                return null;
            }
        }

        public static String OAuthHTMLElementID
        {
            get
            {
                return null;
            }
        }

        public TwitchClient(String channel,String username=null, String password="blah") : base(channel,username,password)
        {
            //Metadata
            identity = new IrcUserRegistrationInfo();
            if (username == null)
            {
                //Use guest account
                username = "justinfan" + new Random().Next(410000000, 420000000);
            }
            identity.UserName = username;
            identity.NickName = username;
            identity.Password = password;
            channelName = channel;
            connection = new IrcClient();
            connection.Connected += onConnected;
            connection.Error += onError;
            connection.Registered += onRegistered;
            connection.PongReceived += onBeatServer;
            connection.Connect("irc.twitch.tv", false, identity);
            //Lists
            users = new List<User>();
        }

        public override bool IsOnChat
        {
            get { return channel != null; }
        }

        public override bool IsGuest
        {
            get { return this.Username.Count()>8 && this.Username.Substring(0, 9) == "justinfan"; }
        }

        public override void SendMessageToChannel(String message)
        {
            connection.LocalUser.SendMessage(channel, message);
        }

        public override void Disconnect()
        {
            /*connection.Disconnect();
            channel = null;
            channelName = null;*/
        }

        public override User GetUser(string username)
        {
            return GetUser(username, false);
        }

        public User GetUser(String username,bool nullOnNotFound=false)
        {
            foreach (var i in users)
            {
                if (i.UserName == username)
                {
                    refreshModModeOnUser(i);
                    return i;
                }
            }
            if (nullOnNotFound) return null;
            //There isn't the user
            //In normal conditions, this must be an error but Twitch servers are seriously bugged and we can't get a reliable user list, so let's create it
            IrcChannelUser userOnC=null;
            foreach (var i in channel.Users)
            {
                if (i.User.NickName == username)
                {
                    userOnC = i;
                    break;
                }
            }
            if (userOnC == null)
            {
                Console.WriteLine("Non notified user on chat: " + username + " wasn't notified by server");
                return CreateUser(username);
            }
            else
            {
                Console.WriteLine("User appeared in server: " + username + " wasn't on list but have been found now, this should be reported to the bugtracker");
                return CreateUser(userOnC);
            }
        }

        private User CreateUser(IrcChannelUser cUser)
        {
            return CreateUser(cUser.User.NickName, cUser);
        }

        private User CreateUser(String username,IrcChannelUser cUser=null)
        {
            var user = GetUser(username, true);
            if (user == null)
            {
                user = new User();
                user.UserName = username;
                //Mod phase
                refreshModModeOnUser(user, cUser);
                //Color default assigment
                var index = (user.UserName[0] + user.UserName[user.UserName.Count() - 1]) % nonProColors.Count;
                user.ColorText = colorsPalette[nonProColors[index]];
                users.Add(user);
                return user;
            }
            else
            {
                Console.WriteLine("Collision of users: " + cUser.User.NickName + " was on the list and someone tried to create him again");
                return user;
            }
        }

        private void RemoveUser(String username)
        {
            var user = GetUser(username, true);
            if (user != null)
            {
                users.Remove(user);
            }
            else
            {
                Console.WriteLine("User dissapeared: " + username + " wasn't on the list when we tried remove it");
            }
        }

        private void refreshModModeOnUser(User user,IrcChannelUser userOnC=null)
        {
            if (userOnC == null)
            {
                foreach (var i in channel.Users)
                {
                    if (i.User.NickName == user.UserName)
                    {
                        userOnC = i;
                        break;
                    }
                }
            }
            if (userOnC == null) return;
            //Boardcaster mode
            if (user.UserName == channelName)
            {
                if (!user.Modes.Contains("broadcaster")) user.Modes.Add("broadcaster");
            }
            //Mod mode
            if (userOnC.Modes.Contains('o'))
            {
                if (!user.Modes.Contains("mod")) user.Modes.Add("mod");
            }
            else
            {
                if (user.Modes.Contains("mod")) user.Modes.Remove("mod");
            }
        }

        void onRegistered(object sender, EventArgs e)
        {
            //sasadd
            //InfoMessageReceived(IsGuest ? "Logged as guest" : "Logged in succesfully", false);
            connection.SendRawMessage("JTVCLIENT    ");
            //connection.SendRawMessage("TWITCHCLIENT 2\r\n\0\0");
            connection.Channels.Join("#"+channelName);
            connection.LocalUser.MessageReceived += onPrivateMessageReceived;
            raiseLogged(username);
        }

        void onPrivateMessageReceived(object sender, IrcMessageEventArgs e)
        {
            Console.WriteLine(e.Text);
            if (e.Source.Name == "jtv" && e.Text.Substring(0, 10) == "HISTORYEND")
            {
                var channelName = e.Text.Substring(11);
                foreach (var i in connection.Channels)
                {
                    if (i.Name == "#" + channelName)
                    {
                        onJoined(connection.LocalUser, new IrcChannelEventArgs(i));
                        break;
                    }
                }
            }
            else if (e.Source.Name == "jtv" && e.Text.Substring(0, 9) == "USERCOLOR" && channel!=null)
            {
                var separator = e.Text.IndexOf(' ', 10);
                var colorEncoded = e.Text.Substring(separator + 1);
                var owner = this.GetUser(e.Text.Substring(10, separator - 10));
                
                owner.ColorText = (Color)ColorConverter.ConvertFromString(colorEncoded);
            }
            else if (e.Source.Name == "jtv" && e.Text.Substring(0, 11) == "SPECIALUSER" && channel != null)
            {
                var array = e.Text.Substring(12).Split(' ');
                var owner = this.GetUser(array[0]);
                if (!owner.Modes.Contains(array[1])) owner.Modes.Add(array[1]);
                if (array.Count() > 2)
                {
                    Console.WriteLine(e.Text);
                }
            }
            else if (e.Source.Name == "jtv" && e.Text.Substring(0, 9) == "CLEARCHAT" && channel != null)
            {
                raiseMutedUser(this.GetUser(e.Text.Substring(10)));
            }
            else if (e.Source.Name == "jtv" && e.Text.Substring(0, 8) == "EMOTESET" && channel != null)
            {
                var array = e.Text.Substring(9).Split(' ');
                var owner = this.GetUser(array[0]);
                owner.EmoticonsSetsAllowed.Clear();
                String setsEncoded = array[1];
                setsEncoded = setsEncoded.Substring(1);
                setsEncoded = setsEncoded.Substring(0,setsEncoded.Count()-1);
                foreach (var set in setsEncoded.Split(','))
                {
                    if (set == "")
                    {
                        break;
                    }
                    owner.EmoticonsSetsAllowed.Add(int.Parse(set));
                }
            }
            else
            {
                Console.WriteLine(e.Text);
            }
        }

        void onJoined(object sender, IrcChannelEventArgs e)
        {
            channel = e.Channel;
            IsConnected = true;
            channel.MessageReceived += onChannelMessageReceived;
            channel.UsersListReceived += onChannelListReceived;
            //InfoMessageReceived("Joined to channel " + channelName, false);
            raiseJoined(channelName);
        }

        void onChannelUserJoined(object sender, IrcChannelUserEventArgs e)
        {
            CreateUser(e.ChannelUser);
        }

        void onChannelUserLeave(object sender, IrcChannelUserEventArgs e)
        {
            RemoveUser(e.ChannelUser.User.NickName);
        }

        void onChannelListReceived(object sender, EventArgs e)
        {
            //Update our user list
            foreach (var i in channel.Users)
            {
                //Modes research
                if (this.GetUser(i.User.NickName, true) == null)
                {
                    CreateUser(i);
                }
            }
        }

        void onChannelMessageReceived(object sender, IrcMessageEventArgs e)
        {
            raiseChannelMessageReceived(this.GetUser(e.Source.Name), e.Text);
        }

        void onError(object sender, IrcErrorEventArgs e)
        {
            if (e.Error.StackTrace.Contains("Chat_streamer"))
            {
                //It's bug from app. Print it
                Console.WriteLine(e.Error.Message);
                Console.WriteLine(e.Error.StackTrace);
                //InfoMessageReceived("Internal error, please send run this application from cmd and send the log to the developer.\n" + e.ToString(), true);
            }
            else
            {
                Console.WriteLine("IRC Library error");
                Console.WriteLine(e.Error.Message);
                Console.WriteLine(e.Error.StackTrace);
            }
        }

        void onConnected(object sender, EventArgs e)
        {
            raiseConnected();
        }

        protected override void reconnect()
        {
            channel.MessageReceived -= onChannelMessageReceived;
            channel.UsersListReceived -= onChannelListReceived;
            channel = null;
            connection.Connected -= onConnected;
            connection.Error -= onError;
            connection.Registered -= onRegistered;
            connection.PongReceived -= onBeatServer;
            connection.Disconnect();
            connection = new IrcClient();
            users.Clear();
            connection.Connected += onConnected;
            connection.Error += onError;
            connection.Registered += onRegistered;
            connection.PongReceived += onBeatServer;
            connection.Connect("irc.twitch.tv", false, identity);
        }

        protected override void sendPing()
        {
            connection.Ping();
        }

        private void onBeatServer(object sender, IrcPingOrPongReceivedEventArgs e)
        {
 	        pongReceived=true;
        }
    }
}
