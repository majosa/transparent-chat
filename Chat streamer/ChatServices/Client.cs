﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Timers;
using System.Windows.Media;
using IrcDotNet;
using System.Windows;

namespace Chat_streamer.ChatServices
{
    public enum UserMode
    {
        User, Mod, Admin, Boardcaster, Staff, Subscriptor
    }

    public class User
    {
        public String UserName;
        public Color ColorText;
        public List<String> Modes;
        public List<int> EmoticonsSetsAllowed;
        public User()
        {
            Modes = new List<string>();
            EmoticonsSetsAllowed = new List<int>();
        }
    }

    public abstract class Client : DependencyObject
    {
        private static List<Type> listClientsModule = new List<Type>();

        protected String channelName;
        protected String username;

        protected Timer watchdog;
        private int periodElapsedForPing;
        private int periodElapsedForPong;
        private bool pingSent;
        protected bool pongReceived;

        public event Action<User, String> ChannelMessageReceived;
        public event Action Connected;
        public event Action<String> Logged;
        public event Action<String> Joined;
        public event Action<User> MutedUser;
        public event Action<Exception> InternalErrorOcurred;


        public bool IsConnected
        {
            get
            {
                bool r = false;
                this.Dispatcher.BeginInvoke(new Action(delegate()
                {
                    r=(bool)GetValue(IsConnectedProperty);
                }), new Object[] {}).Wait();
                return r;
            }
            protected set
            {
                this.Dispatcher.BeginInvoke(new Action<bool>(delegate(bool v)
                {
                    this.SetValue(Client.IsConnectedPropertyKey, v);
                }), new Object[] { value }).Wait();
            }
        }

        public static string ServiceName
        {
            get
            {
                return "Unknown service";
            }
        }

        public static bool IsRequieredOAuth
        {
            get
            {
                return false;
            }
        }

        public static String OAuthPath
        {
            get
            {
                return null;
            }
        }

        public static String OAuthHTMLElementID
        {
            get
            {
                return null;
            }
        }

        // Using a DependencyProperty as the backing store for IsConnected.  This enables animation, styling, binding, etc...
        protected static readonly DependencyPropertyKey IsConnectedPropertyKey =
            DependencyProperty.RegisterReadOnly("IsConnected", typeof(bool), typeof(Client), new PropertyMetadata(false));

        public static readonly DependencyProperty IsConnectedProperty=IsConnectedPropertyKey.DependencyProperty;

        public Client(String channel,String username=null, String password="")
        {
            this.username = username;
            this.channelName = channel;
            //Add dummy methods
            Connected += new Action(delegate()
            {
                watchdog = new Timer(1 * 1000);
                watchdog.Elapsed += onCheckWatchdog;
                watchdog.Start();
            });
            Logged += new Action<string>(delegate(string a) { });
            Joined += new Action<string>(delegate(string a) { });
            ChannelMessageReceived += new Action<User, String>(delegate(User a, String b) { });
        }

        void onBeatServer(object sender, IrcPingOrPongReceivedEventArgs e)
        {
            pongReceived = true;
        }

        public abstract bool IsOnChat
        {
            get;
        }

        public virtual bool IsGuest
        {
            get { return false; }
        }

        public abstract void SendMessageToChannel(String message);

        public virtual void Disconnect()
        {
            IsConnected = false;
            /*connection.Disconnect();
            channel = null;
            channelName = null;*/
        }

        public String Username
        {
            get { return username; }
        }

        public String ChannelName
        {
            get { return channelName; }
        }

        public abstract User GetUser(String username);

        protected abstract void reconnect();

        protected abstract void sendPing();

        #region Watchdog
        void onCheckWatchdog(object sender, ElapsedEventArgs e)
        {
            if (IsConnected)
            {
                watchdog.Stop();
                return;
            }
            if (pingSent)
            {
                if (pongReceived)
                {
                    pingSent = false;
                    pongReceived = false;
                    periodElapsedForPong = 0;
                }
                else if (periodElapsedForPong > 3)
                {
                    //InfoMessageReceived("Connection lost", true);
                    watchdog.Stop();
                    reconnect();
                    pingSent = false;
                    pongReceived = false;
                    periodElapsedForPong = 0;
                }
                else
                {
                    periodElapsedForPong++;
                }
            }
            else
            {
                if (periodElapsedForPing > 3)
                {
                    periodElapsedForPing = 0;
                    sendPing();
                    pingSent = true;
                }
                else
                {
                    periodElapsedForPing++;
                }
            }
        }
        #endregion
        

        protected void raiseChannelMessageReceived(User owner, String msg)
        {
            ChannelMessageReceived(owner, msg);
        }
        protected void raiseConnected()
        {
            Connected();
        }
        protected void raiseLogged(String channel){
            Logged(channel);
        }
        protected void raiseJoined(String channel)
        {
            Joined(channel);
        }
        protected void raiseMutedUser(User mutted)
        {
            MutedUser(mutted);
        }
        protected void raiseInternalErrorOcurred(Exception ex)
        {
            InternalErrorOcurred(ex);
        }

        public static List<Type> GetClients()
        {
            if (listClientsModule.Count <= 0)
            {
                foreach (var i in Assembly.GetCallingAssembly().GetTypes())
                {
                    if (i.IsClass && i.IsSubclassOf(typeof(Client)))
                    {
                        listClientsModule.Add(i);
                    }
                }
            }
            return listClientsModule;
        }

        ~Client()
        {
            this.Disconnect();
        }
    }
}
