﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Windows;

namespace Chat_streamer.ChatServices
{
    public class Emoticon{
        public int? idSet;
        public Size Size;
        public Uri Path;
        public BitmapSource Image;
    }

    public class EmoticonSet
    {
        public Regex Regex;
        public List<Emoticon> Emoticons;

        public EmoticonSet()
        {
            Emoticons = new List<Emoticon>();
        }
    }

    public abstract class Emoticons
    {
        protected List<EmoticonSet> emoticonSets;
        public Emoticons(String channel)
        {
            emoticonSets = new List<EmoticonSet>();
        }

        public Dictionary<Regex, Emoticon> GetList(List<int> usingSets = null)
        {
            Dictionary<Regex, Emoticon> emos = new Dictionary<Regex, Emoticon>();
            if (usingSets == null)
            {
                usingSets = new List<int>();
            }
            foreach (var set in emoticonSets)
            {
                Emoticon choosedEmoticon=null;
                foreach (var element in set.Emoticons)
                {
                    if (choosedEmoticon == null)
                    {
                        if (element.idSet == null)
                        {
                            choosedEmoticon = element;
                            continue;
                        }
                        else if (usingSets.Contains((int)element.idSet))
                        {
                            choosedEmoticon = element;
                            continue;
                        }
                    }
                    else
                    {
                        if (choosedEmoticon.idSet == null && element.idSet != null && usingSets.Contains((int)element.idSet))
                        {
                            choosedEmoticon = element;
                            continue;
                        }
                        else
                        {
                            //Console.WriteLine("Conflict with emoticons sets for " + set.Regex.ToString() + " regex with original set " + choosedEmoticon.idSet + " and found set " + element.idSet + ". The using sets are " + usingSets.ToString());
                        }
                    }
                }
                if (choosedEmoticon != null) emos.Add(set.Regex, choosedEmoticon);
            }
            return emos;
        }

    }
}
