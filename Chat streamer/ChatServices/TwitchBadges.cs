﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Net;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;

namespace Chat_streamer.ChatServices
{
    public class TwitchBadges : Badges
    {
        enum BadgeType
        {
            Mod, Admin, Boardcaster, Staff, Subscriptor
        }
        public TwitchBadges(String channel) : base(channel)
        {
            WebClient request = new WebClient();
            request.OpenReadCompleted += onFileOpened;
            request.OpenReadAsync(new Uri("https://api.twitch.tv/kraken/chat/" + channel + "/badges"));
        }

        void openResource(String badge, String url)
        {
            DownloaderArgs args = new DownloaderArgs();
            args.cache = BitmapCacheOption.OnDemand;
            args.options = BitmapCreateOptions.None;
            args.downloadedHandle = delegate(object sender, EventArgs ev)
            {
                badges.Add(badge, ((BitmapDecoder)sender).Frames[0]);
            };
            args.path = new Uri(url);
            DownloadersQueue.AddToQueue(args);
        }

        void onFileOpened(object sender, OpenReadCompletedEventArgs e)
        {
            JObject doc = (JObject)JObject.Load(new JsonTextReader(new StreamReader(e.Result)));
            foreach (var element in doc)
            {
                if (element.Key == "mod") continue; //Outdated icon
                if (element.Key[0] == '_') continue; //Autoreference node
                if (!element.Value.HasValues) continue; //In subscriber node can appear this case
                JToken image;
                if (((JObject)element.Value).TryGetValue("alpha", out image))
                {
                    openResource(element.Key, image.Value<string>());
                }
                else
                {
                    openResource(element.Key, element.Value["image"].Value<string>());
                }
            }
            //JSON keeps outdated, use real badges
            openResource("mod", "http://www-cdn.jtvnw.net/images/xarth/g/g18_sword-FFFFFF80.png");
            openResource("ign", "http://www-cdn.jtvnw.net/images/xarth/g/g18_ign-00000080.png");
        }
        public override BitmapSource GetBadge(String name)
        {
            if (name == "subscriber") name = "chansub";
            return base.GetBadge(name);
        }
    }
}
