﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;

namespace Chat_streamer
{
    public class InfoMessage : Paragraph
    {
        private InstanceRun instance;
        private Run elem;
        private String key;
        private String[] parameters;

        public InfoMessage(InstanceRun ins, String keyString, String[] param = null)
            : base()
        {
            instance = ins;
            key = keyString;
            parameters = (param == null) ? new String[]{} : param;
            updateText();
            DependencyPropertyDescriptor.FromProperty(Configuration.LanguageProperty, typeof(Configuration)).AddValueChanged(instance.Configuration, new EventHandler(delegate(Object sender, EventArgs ev)
            {
                updateText();
            }));
        }

        public void updateText()
        {
            var ob = instance.StringTables[key];
            if (ob is String)
            {
                elem.Text = ((String)ob);
            }
            else if (ob is Run)
            {
                elem = (Run)ob;
            }
            elem.Text=String.Format(elem.Text, parameters);
            this.Inlines.Clear();
            this.Inlines.Add(elem);
        }
    }
}
