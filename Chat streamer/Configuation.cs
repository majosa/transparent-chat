﻿using System;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Globalization;

namespace Chat_streamer
{
    [Serializable]
    public class Configuration : DependencyObject,ISerializable
    {
        public static DependencyProperty LastUsernameProperty = DependencyProperty.Register("LastUsername", typeof(String), typeof(Configuration),
           new PropertyMetadata());
        public static DependencyProperty LastChannelsProperty = DependencyProperty.Register("LastChannels", typeof(List<String>), typeof(Configuration),
           new PropertyMetadata());
        public static DependencyProperty IsHookCaptureEnabledProperty = DependencyProperty.Register("IsHookCaptureEnabled", typeof(bool), typeof(Configuration),
           new PropertyMetadata());
        public static DependencyProperty IsPreviewsEnabledProperty = DependencyProperty.Register("IsPreviewsEnabled", typeof(bool), typeof(Configuration),
           new PropertyMetadata());
        public static DependencyProperty TextColorProperty = DependencyProperty.Register("TextColor", typeof(Color), typeof(Configuration),
           new PropertyMetadata(Colors.Black));
        public static DependencyProperty BorderTextColorProperty = DependencyProperty.Register("BorderTextColor", typeof(Color), typeof(Configuration),
           new PropertyMetadata(Colors.White));
        public static DependencyProperty TextSizeProperty = DependencyProperty.Register("TextSize", typeof(double), typeof(Configuration),
           new PropertyMetadata(22.0));
        public static DependencyProperty FontProperty = DependencyProperty.Register("Font", typeof(FontFamily), typeof(Configuration),
           new PropertyMetadata(new FontFamily("default")));
        public static DependencyProperty BackgroundColorProperty = DependencyProperty.Register("BackgroundColor", typeof(Color), typeof(Configuration),
           new PropertyMetadata());
        public static DependencyProperty InactiveBackgroundColorProperty = DependencyProperty.Register("InactiveBackgroundColor", typeof(Color), typeof(Configuration),
           new PropertyMetadata());
        public static DependencyProperty BorderColorProperty = DependencyProperty.Register("BorderColor", typeof(Color), typeof(Configuration),
           new PropertyMetadata(Colors.Gray));
        public static DependencyProperty BorderSizeProperty = DependencyProperty.Register("BorderSize", typeof(Thickness), typeof(Configuration),
           new PropertyMetadata(new Thickness(3.0)));
        public static DependencyProperty MaxMessagesProperty = DependencyProperty.Register("MaxMessages", typeof(int), typeof(Configuration),
           new PropertyMetadata(30));
        public static DependencyProperty LanguageProperty = DependencyProperty.Register("Language", typeof(Language), typeof(Configuration),
           new PropertyMetadata(LanguageManager.DefaultLanguage));
        public static DependencyProperty IsTranslationEnabledProperty = DependencyProperty.Register("IsTranslationEnabled", typeof(bool), typeof(Configuration),
           new PropertyMetadata(false));
        public static DependencyProperty PreferedTranslatorModuleProperty = DependencyProperty.Register("PreferedTranslatorModule", typeof(String), typeof(Configuration),
           new PropertyMetadata(Translator.TranslatorManager.DefaultNameTranslator));


        public String Password;


        public Configuration()
        {
            BackgroundColor = Color.FromScRgb(0.5f,1.0f,1.0f,1.0f);
            TextColor = Colors.Black;
            LastChannels = new List<string>();
            //LastChannels.Add("yobavideogames");
            MaxMessages = 30;
        }
        protected Configuration(SerializationInfo info, StreamingContext context):base()
        {
            LastUsername = getObjectFromSI<String>("LastUsername", info);
            LastChannels = getObjectFromSI<List<String>>("LastChannels", info);
            IsHookCaptureEnabled = getObjectFromSI<bool>("IsHookCaptureEnabled",info);
            IsPreviewsEnabled = getObjectFromSI<bool>("IsPreviewsEnabled", info);
            TextColor = (Color)ColorConverter.ConvertFromString(getObjectFromSI<String>("TextColor",info));
            BorderTextColor = (Color)ColorConverter.ConvertFromString(getObjectFromSI<String>("BorderTextColor", info,BorderTextColor.ToString()));
            BackgroundColor = (Color)ColorConverter.ConvertFromString(getObjectFromSI<String>("BackgroundColor",info));
            InactiveBackgroundColor = (Color)ColorConverter.ConvertFromString(getObjectFromSI<String>("InactiveBackgroundColor", info,InactiveBackgroundColor.ToString()));
            MaxMessages = getObjectFromSI<int>("MaxMessages",info,MaxMessages);
            Language = LanguageManager.getLanguage(getObjectFromSI<String>("LangCode", info, LanguageManager.DefaultLanguage.CulturalInfo.TwoLetterISOLanguageName));
            IsTranslationEnabled = getObjectFromSI<bool>("IsTranslationEnabled", info, IsTranslationEnabled);
        }
        private T getObjectFromSI<T>(String name,SerializationInfo info,T def=default(T))
        {
            try
            {
                return (T)info.GetValue(name, typeof(T));
            }
            catch
            {
                Console.WriteLine("Property "+name+" cannot be unserialized");
            }
            return def;
        }
        public static Configuration Load()
        {
            var storage = IsolatedStorageFile.GetUserStoreForAssembly();
            if (storage.FileExists("Settings.bin"))
            {
                var streamBin = storage.OpenFile("Settings.bin", System.IO.FileMode.Open);
                BinaryFormatter serializator = new BinaryFormatter();
                var config=(Configuration)serializator.Deserialize(streamBin);
                streamBin.Close();
                return config;
            }
            else
            {
                return new Configuration();
            }
        }

        public void Save()
        {
            var storage = IsolatedStorageFile.GetUserStoreForAssembly();
            var streamBin = storage.OpenFile("Settings.bin", System.IO.FileMode.Create);
            BinaryFormatter serializator = new BinaryFormatter();
            serializator.Serialize(streamBin,this);
            streamBin.Close();
        }

        public static void SaveDefaultConfig()
        {
            var storage = IsolatedStorageFile.GetUserStoreForAssembly();
            var streamBin = storage.OpenFile("Settings.bin", System.IO.FileMode.Create);
            BinaryFormatter serializator = new BinaryFormatter();
            serializator.Serialize(streamBin, InstanceRun.GetMainInstance().Configuration);
            streamBin.Close();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("LastUsername", LastUsername);
            info.AddValue("LastChannels", LastChannels);
            info.AddValue("IsHookCaptureEnabled", IsHookCaptureEnabled);
            info.AddValue("IsPreviewsEnabled", IsPreviewsEnabled);
            info.AddValue("TextColor",TextColor.ToString());
            info.AddValue("BorderTextColor", BorderTextColor.ToString());
            info.AddValue("BackgroundColor", BackgroundColor.ToString());
            info.AddValue("InactiveBackgroundColor", InactiveBackgroundColor.ToString());
            info.AddValue("MaxMessages", MaxMessages);
            info.AddValue("LangCode", Language.CulturalInfo.TwoLetterISOLanguageName);
        }

        public String LastUsername
        {
            get { return (String)this.GetValue(Configuration.LastUsernameProperty); }
            set { this.SetValue(Configuration.LastUsernameProperty, value); }
        }
        public List<String> LastChannels
        {
            get { return (List<String>)this.GetValue(Configuration.LastChannelsProperty); }
            set { this.SetValue(Configuration.LastChannelsProperty, value); }
        }
        public bool IsHookCaptureEnabled
        {
            get { return (bool)this.GetValue(Configuration.IsHookCaptureEnabledProperty); }
            set { this.SetValue(Configuration.IsHookCaptureEnabledProperty, value); }
        }
        public bool IsPreviewsEnabled
        {
            get { return (bool)this.GetValue(Configuration.IsPreviewsEnabledProperty); }
            set { this.SetValue(Configuration.IsPreviewsEnabledProperty, value); }
        }
        public Color TextColor
        {
            get { return (Color)this.GetValue(Configuration.TextColorProperty); }
            set { this.SetValue(Configuration.TextColorProperty, value); }
        }
        public Color BorderTextColor
        {
            get { return (Color)this.GetValue(Configuration.BorderTextColorProperty); }
            set { this.SetValue(Configuration.BorderTextColorProperty, value); }
        }
        public double TextSize
        {
            get { return (double)this.GetValue(Configuration.TextSizeProperty); }
            set { this.SetValue(Configuration.TextSizeProperty, value); }
        }
        public FontFamily Font
        {
            get { return (FontFamily)this.GetValue(Configuration.FontProperty); }
            set { this.SetValue(Configuration.FontProperty, value); }
        }
        public Color BackgroundColor
        {
            get { return (Color)this.GetValue(Configuration.BackgroundColorProperty); }
            set { this.SetValue(Configuration.BackgroundColorProperty, value); }
        }
        public Color InactiveBackgroundColor
        {
            get { return (Color)this.GetValue(Configuration.InactiveBackgroundColorProperty); }
            set { this.SetValue(Configuration.InactiveBackgroundColorProperty, value); }
        }
        public Color BorderColor
        {
            get { return (Color)this.GetValue(Configuration.BorderColorProperty); }
            set { this.SetValue(Configuration.BorderColorProperty, value); }
        }
        public Thickness BorderSize
        {
            get { return (Thickness)this.GetValue(Configuration.BorderSizeProperty); }
            set { this.SetValue(Configuration.BorderSizeProperty, value); }
        }
        public int MaxMessages
        {
            get { return (int)this.GetValue(Configuration.MaxMessagesProperty); }
            set { this.SetValue(Configuration.MaxMessagesProperty, value); }
        }
        public Language Language
        {
            get { return (Language)this.GetValue(Configuration.LanguageProperty); }
            set { this.SetValue(Configuration.LanguageProperty, value); }
        }
        public bool IsTranslationEnabled
        {
            get { return (bool)this.GetValue(Configuration.IsTranslationEnabledProperty); }
            set { this.SetValue(Configuration.IsTranslationEnabledProperty, value); }
        }
        public String PreferedTranslatorModule
        {
            get { return (String)this.GetValue(Configuration.PreferedTranslatorModuleProperty); }
            set { this.SetValue(Configuration.PreferedTranslatorModuleProperty, value); }
        }
    }
}
