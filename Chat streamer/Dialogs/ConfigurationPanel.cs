﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Chat_streamer.Dialogs
{
    public abstract class ConfigurationPanel : UserControl
    {
        protected InstanceRun instance;

        public ConfigurationPanel(InstanceRun ins) : base()
        {
            instance = ins;
            var t = new ControlTemplate(typeof(ConfigurationPanel));
            t.VisualTree=new System.Windows.FrameworkElementFactory(typeof(ContentPresenter));
            this.Template = t;
        }

        public abstract void LoadFromInstance();

        public abstract void SaveToInstance();

        public virtual String Title
        {
            get { return "Panel"; }
        }
    }
}
