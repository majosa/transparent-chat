﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chat_streamer.Dialogs
{
    /// <summary>
    /// Lógica de interacción para ChannelPicker.xaml
    /// </summary>
    public partial class ChannelPicker : Window
    {
        private InstanceRun instance;
        private bool guest=true;
        private bool channelBoxFocused=false;

        public delegate void ConnectionRequestedHandler(bool isguest);
        public event ConnectionRequestedHandler ConnectionRequested;

        public ChannelPicker(InstanceRun instance,bool signinMode=false)
        {
            guest = !signinMode;
            this.instance = instance;
            this.DataContext = instance;
            this.Resources.MergedDictionaries.Add(instance.StringTables);
            InitializeComponent();
            //Undesign mode

            if (guest)
            {
                SigninGrid.MaxHeight = 0.0;
            }
            else
            {
                SigninButton.MaxWidth = 0.0;
            }
            fillForm();
        }

        private void fillForm()
        {
            if (instance.Username!=null && instance.Username.Substring(0, 9) != "justinfan")
            {
                ChannelBox.Text = instance.Channel;
                UsernameBox.Text = instance.Username;
                PasswordBox.Text = instance.Password;
            }
        }

        private void pushForm()
        {
            instance.Channel = ChannelBox.Text;
            if (!guest)
            {
                instance.Username = UsernameBox.Text;
                instance.Password = PasswordBox.Text;
            }
            else
            {
                instance.Username = "justinfan" + new Random().Next(182900000, 182999999);
                instance.Password = "blah";
            }
        }

        private void refreshChannelListHeight()
        {
            var originalHeight = SigninGrid.MaxHeight;
            SigninGrid.BeginAnimation(ListBox.MaxHeightProperty, null);
            SigninGrid.MaxHeight = 150.0;
            SigninGrid.Measure(new Size(SigninGrid.ActualWidth, (channelBoxFocused) ? 150.0 : 0.0));
            SigninGrid.MaxHeight = originalHeight;
            if (SigninGrid.MaxHeight != SigninGrid.DesiredSize.Height)
            {
                var originalWindowHeight = this.Height - SigninGrid.ActualHeight;
                SigninGrid.BeginAnimation(ListBox.MaxHeightProperty, new DoubleAnimation(SigninGrid.DesiredSize.Height, TimeSpan.FromSeconds(0.3)));
            }
        }

        private void onConnect(object sender, RoutedEventArgs e)
        {
            pushForm();
            this.ConnectionRequested(guest);
            this.Close();
        }

        private void onSignin(object sender, RoutedEventArgs e)
        {
            SigninGrid.BeginAnimation(Grid.MaxHeightProperty, new DoubleAnimation(SigninGrid.ActualHeight, TimeSpan.FromSeconds(0.3)));
            SigninButton.BeginAnimation(Button.WidthProperty, new DoubleAnimation(SigninButton.ActualWidth,0.0, TimeSpan.FromSeconds(0.3)));
            SigninButton.IsEnabled = false;
            guest = false;
        }

        private void onMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void onClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
