﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_streamer.Dialogs
{
    /// <summary>
    /// Lógica de interacción para AppearanceConfigPanel.xaml
    /// </summary>
    public partial class AppearanceConfigPanel : ConfigurationPanel
    {
        public override String Title
        {
            get { return (String)instance.StringTables["Appearance"]; }
        }

        public AppearanceConfigPanel() : base(null)
        {
            InitializeComponent();
        }

        public AppearanceConfigPanel(InstanceRun ins)
            : base(ins)
        {
            InitializeComponent();
            this.DataContext = ins;
            this.Resources.MergedDictionaries.Add(ins.StringTables);
        }

        public override void LoadFromInstance()
        {
            TextColorPicker.SelectedColor = instance.Configuration.TextColor;
            BorderTextColorPicker.SelectedColor = instance.Configuration.BorderTextColor;
            BackgroundColorPicker.SelectedColor = instance.Configuration.BackgroundColor;
            InactiveBackgroundColorPicker.SelectedColor = instance.Configuration.InactiveBackgroundColor;
            BorderColorPicker.SelectedColor = instance.Configuration.BorderColor;
            //((SolidColorBrush)PreviewBox.Foreground).Color = instance.Configuration.TextColor;
            //((SolidColorBrush)PreviewBox.Background).Color = instance.Configuration.BackgroundColor;
            //((SolidColorBrush)PreviewBox.BorderBrush).Color = instance.Configuration.BorderColor;
            FamilySizeBox.Text = instance.Configuration.TextSize.ToString();
            BorderSizeBox.Text = instance.Configuration.BorderSize.Top.ToString();
            FontBox.Text = instance.Configuration.Font.ToString();
        }

        public override void SaveToInstance()
        {
            instance.Configuration.TextColor = TextColorPicker.SelectedColor;
            instance.Configuration.BorderTextColor = BorderTextColorPicker.SelectedColor;
            instance.Configuration.BackgroundColor = BackgroundColorPicker.SelectedColor;
            instance.Configuration.BorderColor = BorderColorPicker.SelectedColor;
            instance.Configuration.TextSize = double.Parse(FamilySizeBox.Text);
            instance.Configuration.InactiveBackgroundColor = InactiveBackgroundColorPicker.SelectedColor;
            instance.Configuration.BorderSize = new Thickness(double.Parse(BorderSizeBox.Text));
            try
            {
                instance.Configuration.Font = new FontFamily(FontBox.Text);
            }
            catch (ArgumentNullException ex)
            {
                instance.Configuration.ClearValue(Configuration.FontProperty);
            }
        }
    }
}
