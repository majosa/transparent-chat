﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using Chat_streamer.ControlTemplates;

namespace Chat_streamer.Dialogs
{
    /// <summary>
    /// Lógica de interacción para ConfigurationDialog.xaml
    /// </summary>
    public partial class ConfigurationDialog : Window
    {
        private InstanceRun instance;
        private List<ConfigurationPanel> panels;
        private ConfigurationPanel panelInUse;
        private bool grid1InUse = false;
        public ConfigurationDialog()
        {
            InitializeComponent();
            composeConfigPanelList();
        }

        public ConfigurationDialog(InstanceRun ins)
        {
            InitializeComponent();
            instance=ins;
            panels = new List<ConfigurationPanel>();
            composeConfigPanelList();
            this.DataContext = instance;
            this.Resources.MergedDictionaries.Add(ins.StringTables);
        }

        private void composeConfigPanelList()
        {
            int rowN = 0;
            foreach (var i in Assembly.GetCallingAssembly().GetTypes())
            {
                if (i.IsClass && i.IsSubclassOf(typeof(ConfigurationPanel)))
                {
                    //Build panel
                    var constructor = i.GetConstructor(new Type[] { typeof(InstanceRun) });
                    var panel=(ConfigurationPanel)constructor.Invoke(new object[]{instance});
                    //Configure panel
                    panel.LoadFromInstance();
                    panel.Foreground = new SolidColorBrush(instance.Configuration.TextColor);
                    panel.Background = new SolidColorBrush(instance.Configuration.BackgroundColor);
                    panel.BorderBrush = new SolidColorBrush(instance.Configuration.BorderColor);
                    //Reserve space on grid
                    var rd = new RowDefinition();
                    rd.Height = GridLength.Auto;
                    PanelsContainer.RowDefinitions.Add(rd);
                    rd = new RowDefinition();
                    rd.Height = GridLength.Auto;
                    PanelsContainer.RowDefinitions.Add(rd);
                    //Create layout
                    var button = new PapperedButton();
                    button.Content = panel.Title;
                    button.IsToggleButton = true;
                    button.Tag = panel;
                    button.Click += onButtonPanelClick;
                    PanelsContainer.Children.Add(button);
                    button.SetValue(Grid.RowProperty, rowN);
                    rowN++;
                    PanelsContainer.Children.Add(panel);
                    panel.Visibility = Visibility.Collapsed;
                    panel.SetValue(Grid.RowProperty, rowN);
                    panel.Tag = button;
                    rowN++;
                    //if (rowN == 2) showPanel(panel);
                }
            }
        }

        private void showPanel(ConfigurationPanel panel)
        {
            panel.Visibility = Visibility.Visible;
            panelInUse = panel;
            panel.LoadFromInstance();
            ((PapperedButton)panel.Tag).IsChecked = true;
        }

        private void hidePanel(ConfigurationPanel panel)
        {
            panel.Visibility = Visibility.Collapsed;
            panelInUse = null;
            ((PapperedButton)panel.Tag).IsChecked = false;
        }

        private void onButtonPanelClick(object sender, RoutedEventArgs e)
        {
            ConfigurationPanel panel = (ConfigurationPanel)((PapperedButton)sender).Tag;
            if (panelInUse == panel)
            {
                hidePanel(panel);
            }
            else if (panelInUse == null)
            {
                showPanel(panel);
            }
            else
            {
                hidePanel(panelInUse);
                showPanel(panel);
            }
        }

        private void onHeaderMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void onApply(object sender, RoutedEventArgs e)
        {
            foreach (var panel in PanelsContainer.Children)
            {
                if (panel is ConfigurationPanel)
                {
                    ((ConfigurationPanel)panel).SaveToInstance();
                }
            }
        }

        private void onCancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void onAccept(object sender, RoutedEventArgs e)
        {
            onApply(sender, e);
            onCancel(sender, e);
        }
    }
}
