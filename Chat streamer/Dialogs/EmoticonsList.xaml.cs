﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chat_streamer.Dialogs
{

    public class EmoticonBox : DependencyObject
    {

        public BitmapSource Source
        {
            get { return (BitmapSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Source.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(BitmapSource), typeof(EmoticonBox), new PropertyMetadata());



        public String Label
        {
            get { return (String)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Label.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(String), typeof(EmoticonBox), new PropertyMetadata(""));

        public EmoticonBox(BitmapSource s, String l)
        {
            this.Source = s;
            this.Label = l;
        }
    }
    /// <summary>
    /// Lógica de interacción para EmoticonsList.xaml
    /// </summary>
    public partial class EmoticonsList : Window
    {
        private InstanceRun instance;

        public EmoticonsList()
        {
            InitializeComponent();
        }

        public EmoticonsList(InstanceRun ins)
        {
            InitializeComponent();
            instance = ins;
            this.DataContext = ins;
            this.Resources.MergedDictionaries.Add(ins.StringTables);
            generateList(ins.Client.GetUser(ins.Username).EmoticonsSetsAllowed);
        }

        private void generateList(List<int> sets=null)
        {
            EmoticonsArea.Items.Clear();
            foreach (var emoticon in instance.Emoticons.GetList(sets))
            {
                String rex = emoticon.Key.ToString();
                rex = rex.Replace("\\s", "");
                EmoticonBox em = new EmoticonBox(emoticon.Value.Image,rex);
                EmoticonsArea.Items.Add(em);
            }
        }

        private void onMove(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void onClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
