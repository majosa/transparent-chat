﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Chat_streamer.Dialogs
{
    /// <summary>
    /// Lógica de interacción para AppearanceConfigPanel.xaml
    /// </summary>
    public partial class LanguageConfigPanel : ConfigurationPanel
    {
        public override String Title
        {
            get { return (String)instance.StringTables["Language"]; }
        }

        public LanguageConfigPanel() : base(null)
        {
            InitializeComponent();
        }

        public LanguageConfigPanel(InstanceRun ins)
            : base(ins)
        {
            InitializeComponent();
            this.DataContext = ins;
            this.Resources.MergedDictionaries.Add(ins.StringTables);
        }

        public override void LoadFromInstance()
        {
            LangBox.Text = instance.Configuration.Language.CulturalInfo.NativeName;
            EnableTranslation.IsChecked = instance.Configuration.IsTranslationEnabled;
            TranslatorBox.Text = instance.Configuration.PreferedTranslatorModule;
        }

        public override void SaveToInstance()
        {
            instance.Configuration.Language = LanguageManager.getLanguageByDisplayName(LangBox.Text);
            instance.Configuration.IsTranslationEnabled = (bool)EnableTranslation.IsChecked;
            instance.Configuration.PreferedTranslatorModule = TranslatorBox.Text;
        }
    }
}
